
from common import MediaProcess

# Originally cribbed from AG startup
vicstartup="""
option add Vic.disable_autoplace %(disable_autoplace)s startupFile
option add Vic.muteNewSources %(muteNewSource)s startupFile
option add Vic.maxbw 6000 startupFile
option add Vic.bandwidth %(bandwidth)d startupFile
option add Vic.framerate %(framerate)d startupFile
option add Vic.quality %(quality)d startupFile
option add Vic.defaultFormat %(defaultFormat)s startupFile
option add Vic.inputType %(inputType)s startupFile
option add Vic.device \"%(device)s\" startupFile
option add Vic.defaultTTL 127 startupFile
option add Vic.rtpName \"%(rtpName)s\" startupFile
option add Vic.rtpEmail \"%(rtpEmail)s\" startupFile
proc user_hook {} {
    global videoDevice inputPort transmitButton transmitButtonState

    update_note 0 \"%(update_note)s\"

    after 200 {
        set transmitOnStartup %(transmitOnStartup)s

        if { ![winfo exists .menu] } {
            build.menu
        }

        if { ![info exists env(VIC_DEVICE)] } {
            set deviceName \"%(deviceName)s\"

            foreach v $inputDeviceList {
                if { [string last $deviceName [$v nickname]] != -1 } {
                    set videoDevice $v
                    select_device $v
                    break
                }
            }
        }
        set inputPort %(inputPort)s
        grabber port %(grabberport)s

        if { $transmitOnStartup } {
            if { [$transmitButton cget -state] != \"disabled\" } {
                set transmitButtonState 1
                transmit
            }
        }
    }
}
"""


def genInitFile(self):
    return vicstartup % {
    'disable_autoplace':'false',
    'muteNewSource':val,
    'bandwidth':val,
    'framerate':val,
    'quality':val,
    'defaultFormat':val,
    'inputType':val,
    'device':val,
    'rtpName':val,
    'rtpEmail':val,
    'update_note':val,
    'transmitOnStartup':val,
    'deviceName':val,
    'inputPort':val,
    'grabberport':val}


class VicProcess(MediaProcess):
    def resourceAdd(self, resource):
        self.log.info("Vic received resource add")
        if resource.media == 'video':
            resource.addReceiver('activate', self.activate)

