
import logging
from wxPython.wx import *
from twisted.internet import reactor
from common.components import View

PRESCOLOR = {'offline':None,
             'online':'green',
             'away': 'yellow',
             'xa': 'blue',
             'dnd': 'red'}

class RosterTree(wxTreeCtrl):
    def __init__(self, parent, roster):
        wxTreeCtrl.__init__(self, parent, wxNewId(),
                            style=wxTR_HAS_BUTTONS | wxTR_LINES_AT_ROOT | wxTR_HIDE_ROOT)

        self.root = self.AddRoot("Root Item")
        self.Expand(self.root)

        self.roster = roster
        roster.addReceiver('gotRoster', self.gotRoster)
        roster.addReceiver('gotPresence', self.gotContactPresence)

        self.log = logging.getLogger("gui")


    def gotContactPresence(self, user):
        self.log.info("GUI got presence for user "+user.name+'='+user.presence)
        if user.presence not in PRESCOLOR:
            return
        self.SetItemBackgroundColour(user.udat, PRESCOLOR[user.presence])


    def gotRoster(self, roster):
        self.log.info("GUI got roster")
        groups = roster.groups()
        for gname in groups.keys():            
            child = self.AppendItem(self.root, gname or 'Ungrouped')
            for contact in groups[gname]:
                item = self.AppendItem(child, contact.name)
                # Store ref to created row with model
                self.SetPyData(item, contact)
                contact.udat = item
            self.Expand(child)



class AppFrame(wxFrame):
    def __init__(self, app, account, xmpp):
        wxFrame.__init__(self, NULL, -1, 'WX Jabber App', wxDefaultPosition, wxSize(640, 512))

        self.app = app
        self.account = account
        self.xmpp = xmpp

        ### Setup Window ... ###
        self.CreateStatusBar()
        self.Bind(EVT_CLOSE, self.doExit)

        ### Menu ###
        menu = wxMenu()

        ConnId  = wxNewId()
        menu.Append(ConnId, "C&onnect", "Connect to server")
        self.Connect(ConnId, -1, wxEVT_COMMAND_MENU_SELECTED, self.doConnect)

        ExitId  = wxNewId()
        menu.Append(ExitId, "E&xit", "Terminate the program")
        self.Connect(ExitId, -1, wxEVT_COMMAND_MENU_SELECTED, self.doExit)

        menuBar = wxMenuBar()
        menuBar.Append(menu, "&File")
        self.SetMenuBar(menuBar)

        ### Split Window ###
        splitter = wxSplitterWindow (self, -1, style=wxNO_3D|wxSP_3D)
        splitter.SetMinimumPaneSize (1)
        
        ### Roster ###
        self.roster = RosterTree(splitter, account.roster)

        ### Done ###
        splitter.SplitVertically (self.roster, wxPanel(splitter, -1))
        splitter.SetSashPosition (160, True)

        self.Show(true)

        # Operate with the Twisted system
        reactor.interleave(wxCallAfter)

    def message(self, msg):
        self.GetStatusBar().PushStatusText(msg)

    def doConnect(self, event):
        self.message("Connecting...")
        self.xmpp.connect()

    def doExit(self, event):
        if reactor.running:
            reactor.addSystemEventTrigger('after', 'shutdown', self.Close, true)
            reactor.stop()
        else:
            self.app.Exit()


class WxApp(wxApp, View):
    def __init__(self, account, xmpp):
        wxApp.__init__(self, 0)

        self.account = account
        self.xmpp = xmpp        

    def getType(self):
        return 'wx'

    def run(self):
        frame = AppFrame(self, self.account, self.xmpp)
        self.SetTopWindow(frame)
        self.MainLoop()
