#
# XMPP 'Adventure' client.
#
import sys, logging, weakref
from string import split, rstrip
from twisted.words.xish import xpath
from common.utils import SignalEmitter
from common.model import ChatMessage, Conversation
from vlxmpp.xmpp import Stanza, IQ, Presence, XMessageMix, XNS_CLIENT, XNS_XDATA, XNS_PRIVATE
from vlxmpp.utils import xmlToForm, formToXml
import plugins.chat

class XChatMessage(ChatMessage, XMessageMix):
    def __init__(self, xml):
        ChatMessage.__init__(self, tx=False)
        XMessageMix.__init__(self, xml)

class ChatXMPP(SignalEmitter):
    def __init__(self, core, plugin):
        SignalEmitter.__init__(self, ['rxinvite'])
	self.plugin = plugin
	self.core = core
        self.account = core.account
        self.xmpp = core.xmpp
        self.log = plugins.chat.log

        self.account.conversations.addReceiver('create', self.newConversation)
        self.xmpp.addReceiver('chatmsg', self.rxMessage)

    def newConversation(self, conv):
        self.log.info("Received conversation init signal "+conv.type)
        if conv.type != 'chat':
            return
        self.log.info("@@@@@ Received conversation init signal "+conv.type)
        conv.addReceiver('txChatMsg', self.txMessage)

    def txMessage(self, conv):
        self.log.info("Received outgoing message")
        m = Stanza((XNS_CLIENT,"message"), attribs={'to':conv.target.full(),
                                                    'type':'chat'})
        m.addStanza("body", content=conv.lastMsg().txt)
        self.xmpp.send(m)

    def rxMessage(self, msg):
        self.log.debug("Chat got Message")

        body = xpath.queryForNodes('/message/body', msg)

        if not body:
            # FIXME: Other message info (events, etc)
            return

        # FIXME: This assumes only a single body in the
        # message, is this valid?
        #chat = ChatMessage(str(body[0]), sender=sender, tx=False)
        chat = XChatMessage(msg)

        conv = self.account.conversations.getConversation(chat.sender)
        if not conv:
            contact = self.account.roster.getContact(chat.sender)
            if not contact:
                contact = Contact(chat.sender)
            conv = Conversation(self.account, contact)
            self.account.conversations.addConversation(conv)
        conv.addRxMsg(chat)

