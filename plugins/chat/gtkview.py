
import string, socket
from ConfigParser import ConfigParser, NoOptionError, DuplicateSectionError, NoSectionError
import gobject, gtk, gtk.glade, pango, gconf
import gtkui
from gtkui.components import TrivialGTKPlugin, TrivialGTKComponent
from gtkui.chat import ChatView
from gtkui.xform import XFormDialog
from common.model import Contact, Conversation
from common.utils import SignalEmitter
from common.config import PyConfig
from plugins.chat import log

class ChatGtkComponent(ChatView, TrivialGTKComponent):
    def __init__(self, conv):
        TrivialGTKComponent.__init__(self, conv.contact.name)
        ChatView.__init__(self, conv)


class ChatGtkPlugin(TrivialGTKPlugin):
    """Main handler for GUI events not attached to particular chat
       session (e.g) menu items"""
    def __init__(self, plugin):
        TrivialGTKPlugin.__init__(self)
        self.plugin = plugin

        # Roster context items
        self.rosterctxitems.append(self.gen_startchat)
        self.rosterDefaultAction = self.initChat

        # Hook up to main UI
        #self.view = plugin.core.view
        self.account = plugin.core.account
        self.xmpp = plugin.chatxmpp

        self.account.conversations.addReceiver('create', self.newConversation)


    def gen_startchat(self, contact):
        mi = gtk.MenuItem("Start Chat")
        mi.connect('activate', self.on_startchat_activate, contact)
        return mi

    def on_startchat_activate(self, widget, contact):
        self.initChat(contact)

    def initChat(self, contact):
        # Get the conversation for this user.  If this doesn't
        # exist a 'newconversation' signal should result in its
        # creation.
        conv = self.account.conversations.getConversation(contact.jid)
        if not conv:
            conv = Conversation(self.account, contact)
            self.account.conversations.addConversation(conv)
            # Conversation creation triggers window creation via
            # 'newconversation' signal ...


#     def createView(self, conv, xmppcomp):
#         view = MUCGtkComponent(conv, xmppcomp)
#         self.emitsig('create', component=view)
#         return view

    def newConversation(self, conv):
        log.info("Received conversation init signal")
        if conv.type != 'chat':
            return
        
        self.emitsig('create', component=ChatGtkComponent(conv))

