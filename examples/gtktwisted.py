#!/usr/bin/env python

import pygtk
pygtk.require('2.0')
import gtk

from twisted.internet import gtk2reactor
gtk2reactor.install()  # Must be done before other twisted imports
from twisted.internet import reactor
from twisted.xish import xmlstream, xpath
from libsprained import domish
from twisted.words.protocols.jabber import client,jid

class JHandler:
    def __init__(self, jidstr, passwd):
        self.jid = jid.JID(jidstr)
        self.passwd = passwd
        self.stream = None
        self.endsent = False

    def _xrx(self,xml):
        print('RX: '+xml)

    def _xtx(self,xml):
        print("TX:"+xml)

    def connect(self, retry=0):
        print("Connection initiated")
        # Initialise connection and authorisation

        #factory = client.basicClientFactory(self.account.jid, self.account.passwd)
        ba = client.BasicAuthenticator(self.jid, self.passwd)
        factory = xmlstream.XmlStreamFactory(ba)
        #factory.continueTrying = retry  # FIXME: This should work but doesn't

        factory.addBootstrap(xmlstream.STREAM_CONNECTED_EVENT, self.connected)
        factory.addBootstrap(xmlstream.STREAM_AUTHD_EVENT, self.authorised)
        factory.addBootstrap(xmlstream.STREAM_ERROR_EVENT, self.error)
        factory.addBootstrap(xmlstream.STREAM_END_EVENT, self.gotEnd)

        reactor.connectTCP(self.jid.host, 5222, factory)

    
    def connected(self, stream):
        self.stream = stream
        # Debug IO
        stream.rawDataInFn = self._xrx
        stream.rawDataOutFn = self._xtx

    def stop(self):
        print "XMPP stopping"
        self.endstream()

    def endstream(self, stream):
        self.stream.send('</stream:stream>')
        self.endsent = True

    def gotEnd(self, stream):
        print("Received end of stream from server")
        if not self.endsent:
            self.endstream(self)

    def authorised(self, stream):
        print "Got authorised"

    def error(self,err):
        print "Got error"



class GtkJabberTest:

    def quit(self, widget, data=None):
        #self.xmpp = xmpp
        #gtk.main_quit()
        reactor.stop()

    def __init__(self, xmpp):
        self.xmpp = xmpp

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("destroy", self.quit)
        self.window.set_border_width(10)
        self.button = gtk.Button("Quit GTK/Twisted")    
        self.button.connect("clicked", self.quit, None)
        self.window.add(self.button)
        self.window.show_all()

xmpp = JHandler("tarka@moth.vislab.usyd.edu.au/TestClient", "wibble")
test = GtkJabberTest(xmpp)
xmpp.connect()
reactor.run()
