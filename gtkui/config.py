
import pygtk
pygtk.require('2.0')
import gtk

GLADEFILE = 'gtkui/glade/jabber-gtk.glade'

_widgets = None
def getWidgets():
    global _widgets
    if not _widgets:
        _widgets = gtk.glade.XML(GLADEFILE)
    return _widgets


PRESTYPES = (('offline', 'Offline'),
             ('online', 'Available'),
             ('away', 'Away'),
             ('xa', 'Extended Away'),
             ('dnd', 'Do Not Disturb'))

ICONROOT = 'icons/signs/'
ICONSIZE = 16
PRESICONS = {'offline':'dnd.png',
             'online':'available.png',
             'away':'away.png',
             'xa':'xa.png',
             'dnd':'busy.png'}
def iconPath(short):
    return "%s/%sx%s/%s"%(ICONROOT,ICONSIZE,ICONSIZE,PRESICONS[short])

icons = None
def getIcons():
    global icons
    if not icons:
        from gtk import Image
        icons = {}
        for stat in PRESTYPES:
            short = stat[0]
            img = Image()
            img.set_from_file(iconPath(short))
            icons[short] = img
    return icons



PRESCOLOR = {'offline':None,
             'online':'green',
             'away': 'yellow',
             'xa': 'blue',
             'dnd': 'red'}

PALETTE = ["#FF0000",
           "#0000FF",
           "#490302",
           "#444d99",
           "#e77e00",
           "#c58551",
           "#d8e7f1",
           "#d6f8c4",
           "#2c5c17"]
