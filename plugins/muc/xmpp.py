#
# XMPP 'Adventure' client.
#
import sys, logging, weakref
from string import split, rstrip
from twisted.words.xish import xpath
from common.utils import SignalEmitter
from vlxmpp.xmpp import Stanza, IQ, Presence, XMessageMix, XNS_CLIENT, XNS_XDATA, XNS_PRIVATE
from vlxmpp.utils import xmlToForm, formToXml
from muc.model import MucJID, MUCMessage, MUCMember, MUCService, MucInviteRX

LOGNAME = 'muc'

XNS_MUC = 'http://jabber.org/protocol/muc'
XNS_MUC_USER = XNS_MUC+'#user'
XNS_MUC_OWNER = XNS_MUC+'#owner'
XNS_MUC_SDP = "http://vislab.usyd.edu.au/protocol/muc#sdp"
XNS_XSDP = 'http://vislab.usyd.edu.au/protocol/xsdp'


class XMucMessage(MUCMessage, XMessageMix):
    def __init__(self, xml):
        MUCMessage.__init__(self, tx=False)
        XMessageMix.__init__(self, xml)


class MucXMPP(SignalEmitter):
    def __init__(self, core, plugin):
        SignalEmitter.__init__(self, ['rxinvite'])
	self.plugin = plugin
	self.core = core
        self.account = core.account
        self.xmpp = core.xmpp
        self.log = logging.getLogger(LOGNAME)

        #acct.conversations.addReceiver('create', self.createMUC)
        self.xmpp.addReceiver('message', self.rxMessage)  # For invites

    def createMUC(self, conv):
        self.log.info("Received MUC init")
        if conv.type != 'groupchat':
            return
        return MucComponentXMPP(conv, self.xmpp)


    #################### Invitations ####################

    # <message xmlns='jabber:client' type='normal'
    # 	 to='ssmith@jabber.vislab.usyd.edu.au'
    # 	 from='vislab@conference.vislab.usyd.edu.au'>
    #   <x xmlns='http://jabber.org/protocol/muc#user'>
    #    <invite from='tarka@moth.vislab.usyd.edu.au/Gaim'>
    # 	   <reason>Test invite</reason>
    #    </invite>
    #  </x>
    #  <x xmlns='jabber:x:conference'
    # 	jid='vislab@conference.vislab.usyd.edu.au'>
    # 	Test invite
    #  </x>
    # </message>
    def rxMessage(self, msg):
        self.log.debug("MUC Factory got Message")

        xp = "//x[@xmlns='%s']/invite" % XNS_MUC_USER        
        invite = xpath.queryForNodes(xp, msg)
        if not invite:
            return
        invite = invite[0].children[0]  # FIXME: This shouldn't be necessary
        reason = xpath.queryForNodes('/invite/reason', invite)
        if reason:
            reason = str(reason[0])
        else:
            reason = ''

        room = MucJID(msg.attributes['from'])
        invitor = MucJID(invite.attributes['from'])
        self.log.info("Got invite to %s from %s", room.full(), invitor.full())

        invite = MucInviteRX(room, invitor, reason=reason)
        self.emitsig('rxinvite', invite=invite)


    # <message
    #     from='crone1@shakespeare.lit/desktop'
    #     to='darkcave@macbeth.shakespeare.lit'>
    #   <x xmlns='http://jabber.org/protocol/muc#user'>
    #     <invite to='hecate@shakespeare.lit'>
    #       <reason>
    #         Hey Hecate, this is the place for all good witches!
    #       </reason>
    #     </invite>
    #   </x>
    # </message>
    def sendInvite(self, conv, contact, reason=None):
        m = Stanza((XNS_CLIENT,"message"), attribs={'to':conv.target.userhost()})
        x = m.addStanza('x', xns=XNS_MUC_USER)
        i = x.addStanza('invite', attribs={'to':contact.jid.full()})
        i.addStanza('reason', content=reason)
        self.xmpp.send(m)

    # <message
    #     from='hecate@shakespeare.lit/broom'
    #     to='darkcave@macbeth.shakespeare.lit'>
    #   <x xmlns='http://jabber.org/protocol/muc#user'>
    #     <decline to='crone1@shakespeare.lit'>
    #       <reason>
    #         Sorry, I'm too busy right now.
    #       </reason>
    #     </decline>
    #   </x>
    # </message>
    def declineInvite(self, invite):
        return  # FIXME: Rejected by ejabberd
        m = Stanza((XNS_CLIENT,"message"), attribs={'to':invite.room.userhost()})
        x = m.addStanza('x', xns=XNS_MUC_USER)
        i = x.addStanza('decline', attribs={'to':invite.sender.full()})
        i.addStanza('reason')
        self.xmpp.send(m)


class MucComponentXMPP:
    def __init__(self, conv, xmpp):
        self.conversation = conv
        self.xmpp = xmpp

        self.log = logging.getLogger(LOGNAME)

        # Convenience shortcuts
        self.account = conv.account
        self.roomstr = self.conversation.contact.jid.userhost()

        xmpp.addFeature(XNS_MUC)

        xmpp.addReceiver('iq', self.rxIQ)
        xmpp.addReceiver('presence', self.rxPresence)
        xmpp.addReceiver('mucmsg', self.rxMessage)

        conv.addReceiver('txChatMsg', self.txMUCMsg)
        conv.addReceiver('end', self.gotEnd)

        # Request entry to the room (JEP-45, 6.3.2)
        # <presence
        #     from='hag66@shakespeare.lit/pda'
        #     to='darkcave@macbeth.shakespeare.lit/thirdwitch'>
        #   <x xmlns='http://jabber.org/protocol/muc'/>
        # </presence>
        p = Stanza((XNS_CLIENT, "presence"), attribs={'from':self.account.jid.full(),
                                                      'to':conv.contact.jid.full()})
        p.addStanza('x', xns=XNS_MUC)
        self.xmpp.send(p)

        # Find out about the room, including SDP if there
        self.querySDP()

        self.log.info("New MUC XMPP initialised")


    def rxIQ(self, iq):
        self.log.info("MUC Plugin got IQ")

    def rxPresence(self, sender, pres):
        self.log.info("MUC Plugin got Presence")
        if sender.userhost() != self.conversation.contact.jid.userhost():
            return

        # Decode and add/update roster
        xp = "//x[@xmlns='%s']/item" % XNS_MUC_USER        
        x = xpath.queryForNodes(xp, pres)
        if not x:
            return # FIXME: Send error?
        x = x[0]

        items = xpath.queryForNodes('/x/item', x)
        if not items:
            return # FIXME: Send error?
        
        i = items[0]
        m = MUCMember(sender, i['role'], i['affiliation'])
        if ('type' in pres.attributes) and (pres.attributes['type'] == 'unavailable'):
            self.conversation.roster.remove(m)
        else:
            self.conversation.roster.addUpdate(m)


    def rxMessage(self, msg):
        self.log.info("MUC Plugin got Message")
        
        sender = MucJID(msg.attributes['from'])
        if sender.userhost() != self.conversation.contact.jid.userhost():
            return

        self.log.info("Message from %s", msg.attributes['from'])

        # Fetch message bodies (if any)
        # FIXME: Should get jabber:x:delay info and sort accordingly
        bodies = xpath.queryForNodes('/message/body', msg)
        for body in bodies or []:
            m = XMucMessage(msg)
            self.conversation.addRxMsg(m)

        # Subject handling
        subs = xpath.queryForNodes('/message/subject', msg)
        if subs:
            self.conversation.subject = str(subs[0])


    def txMUCMsg(self, conv):
        self.log.info("Received outgoing message")
        m = Stanza((XNS_CLIENT,"message"), attribs={'to':conv.target.userhost(),
                                                    'type':'groupchat'})
        m.addStanza("body", content=conv.lastMsg().txt)
        self.xmpp.send(m)


    def gotEnd(self, conv):
        self.log.info("Received conversation end")
        p = Stanza((XNS_CLIENT, "presence"), attribs={'to':self.conversation.contact.jid.full(),
                                                      'type':'unavailable'})
        p.addStanza('x', xns=XNS_MUC)
        self.xmpp.send(p)

    # <iq from='crone1@shakespeare.lit/desktop'
    #     id='config1'
    #     to='darkcave@macbeth.shakespeare.lit'
    #     type='get'>
    #   <query xmlns='http://jabber.org/protocol/muc#owner'/>
    # </iq>
    def reqConfig(self, targetCB):
        self.log.info("Requesting room configuration")
        iq = IQ('get', {'to':self.conversation.contact.jid.userhost()})
        q = iq.addStanza('query', xns=XNS_MUC_OWNER)

        self.xmpp.IQHandle(iq, self.configCB, target=targetCB)
        self.xmpp.send(iq)

    def cancelConfig(self):
        self.log.info("Cancelling room configuration")
        iq = IQ('set', {'to':self.conversation.contact.jid.userhost()})
        q = iq.addStanza('query', xns=XNS_MUC_OWNER)
        q.addStanza('x', xns=XNS_XDATA, attribs={'to':self.conversation.contact.jid.full(),
                                                 'type':'unavailable'})
        self.xmpp.send(iq)

    def configCB(self, iq, target):
        self.log.info("Got requested room configuration form")
        type = iq.attributes['type']
        if type == 'error':
            self.log.info("Configuration error")
            # FIXME: We should check for the actual error type, but
            # this is the common case:
            target(None, error='forbidden')
        else:
            target(xmlToForm(iq))

    def sendConfig(self, form):
        form.type = 'submit'
        iq = IQ('set', {'to':self.conversation.contact.jid.userhost()})
        q = iq.addStanza('query', xns=XNS_MUC_OWNER)
        q.addChild(formToXml(form))
        self.xmpp.send(iq)

    def sendSubject(self, subj):
        self.log.info("Setting subject")
        m = Stanza((XNS_CLIENT,"message"), attribs={'to':self.conversation.target.userhost(),
                                                    'type':'groupchat'})
        m.addStanza("subject", content=subj)
        self.xmpp.send(m)


    #################### SDP Extensions ####################

    def querySDP(self):
        iq = IQ('get', {'to':self.conversation.contact.jid.userhost()})
        q = iq.addStanza('query', xns=XNS_PRIVATE)
        q.addStanza('cfg', xns=XNS_XSDP)
        self.xmpp.IQHandle(iq, self.sdpCB)
        self.xmpp.send(iq)


    def sdpCB(self, iq):
        self.log.info("Got SDP reply")
        xp = "/iq/query[@xmlns='%s']" % XNS_PRIVATE
        items = xpath.queryForNodes(xp, iq)
        if not items:
            return

        for i in items or []:
            xp = "/query/cfg[@xmlns='%s']" % XNS_XSDP
            cfg = xpath.queryForNodes(xp, i)
            if not cfg:
                return  # Form not ours

            self.gotSDP(cfg[0])


    # Example:
    #
    # 	     <cfg xmlns='http://vislab.usyd.edu.au/protocol/xsdp'>
    # 	      <component media='video' name='AG-video'>
    # 	       <alt name='AG-video-mcast'
    # 		    rtp-port='17018' 
    # 		    ttl='127' 
    # 		    addr='233.2.178.9' 
    # 		    format='rtp-avp-31'/>
    # 	      </component>
    # 	      <component media='video' name='AG-video'>
    # 	       <alt name='AG-video-mcast'
    # 		    rtp-port='17018' 
    # 		    ttl='127' 
    # 		    addr='233.2.178.9' 
    # 		    format='rtp-avp-31'/>
    # 	      </component>
    # 	   </cfg>

    def gotSDP(self, xsdp):
        self.log.info("Received SDP information "+xsdp.toXml())

        xp = "/cfg/component"
        comps = xpath.queryForNodes(xp, xsdp)
        if not comps:
            self.log.warning("No components in SDP information")
            return

        for c in comps:
            self.log.info("Got component "+c.toXml())        
            media = c.attributes['media']
            for alt in xpath.queryForNodes('/component/alt', c) or []:

                rec = MUCService(alt.attributes['name'], 'sdp')
                rec.media = media
                rec.addr = alt.attributes['addr']
                rec.port = alt.attributes['rtp-port']
                rec.ttl = alt.attributes['ttl']
                rec.format = alt.attributes['format']
                enc = xpath.queryForNodes('/alt/encryption', alt)
                if enc:
                    rec.enctype = enc[0].attributes['type']
                    rec.enckey = str(enc[0])
                else:
                    rec.enctype = None
                    rec.enckey = None

                self.log.info("Stream: %s, %s, %s" % (rec.media, rec.addr, rec.port))
                
                self.conversation.services.add(rec)

    def sendSDP(self, reclist):
        self.log.info("Setting SDP information ")
        iq = IQ('set', {'to':self.conversation.target.userhost()})
        q = iq.addStanza('query', xns="jabber:iq:private")
        cfg  = q.addStanza('cfg', xns='http://vislab.usyd.edu.au/protocol/xsdp')

        for rec in reclist:
            c = cfg.addStanza('component', attribs={'media':rec.media, 'name':'AG '+rec.media})
            alt = c.addStanza('alt', attribs={'name':rec.name,
                                              'rtp-port':str(rec.port),
                                              'ttl':str(rec.ttl),
                                              'addr':rec.addr,
                                              'format':rec.format})
        self.xmpp.send(iq)

