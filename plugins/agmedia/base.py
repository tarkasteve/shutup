
import logging
from twisted.internet import reactor
from muc.model import MUCConversation
from common.process import ProcessHandler

class MediaProcess(ProcessHandler):
    service = None
    path = None

    def __init__(self, account):
        ProcessHandler.__init__(self)
        
        self.log = logging.getLogger("process")
        account.conversations.addReceiver('create', self.newMUC)


    def _spawn(self):
        params = self.genParams()
        self.log.info("Process %s starting with %s" % (self.path, params))
        self.proc = reactor.spawnProcess(self, self.path, params,
                                         env=None)


    def newMUC(self, conv):
        self.log.info("Media received MUC init")
        if conv.type != 'groupchat':
            return
        conv.services.addReceiver('add', self.serviceAdd)

    def activate(self, service):
        self.log.info("Media received service activate")

        self.service = service

        if self.proc:
            self.proc.signalProcess('HUP')
            self.queued = True 
        else:
            self._spawn()
