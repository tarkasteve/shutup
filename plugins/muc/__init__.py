
import logging
from common.components import TrivialComponentPlugin, PluginException
from common.model import Contact
import muc
from muc.model import MUCConversation
from muc.xmpp import MucXMPP

class MUCPlugin(TrivialComponentPlugin):
    """Main handler for MUC GUI events not attached to particular chat
       session (e.g) menu items"""

    name = "Multi-User Chat"
    author = "Steve Smith <ssmith@vislab.usyd.edu.au>"
    desc = "Add's Multi-User chat functionality"
    handle = "muc"

    def __init__(self, core):
        TrivialComponentPlugin.__init__(self)
        self.core = core

        self.mucxmpp = MucXMPP(core, self)

        uitype = core.view.getType()
        if uitype == 'gnome' or uitype == 'gtk':
            from muc.gtkview import MUCGTKPlugin
            self.viewplugin = MUCGTKPlugin(self)
        else:
            log.warn('MUC plugin does not support view-type %s, disabling' % core.view.getType())
            raise PluginException()
        core.view.addComponentPlugin(self.viewplugin)

        log.info('Initialised MUC plugin')

    def createComponent(self, jid):
        # Just check it's not there already
        account = self.core.account
        if account.conversations.getConversation(jid):
            return 

        conv = MUCConversation(account, Contact(jid))
        account.conversations.addConversation(conv)

        xmppcomp = self.mucxmpp.createMUC(conv)

        muc = self.viewplugin.createView(conv, xmppcomp)


def init(coreapp):
    global log
    log = logging.getLogger("muc")

    try:
        return MUCPlugin(coreapp)
    except(PluginException):
        return None
