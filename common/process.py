
from twisted.internet import reactor
from twisted.internet.protocol import ProcessProtocol


class ProcessHandler(ProcessProtocol):
    def __init__(self):
        self.proc = None
        self.queued = False
        
    def _spawn(self):
        assert False,"_spawn in base class called, this shouldn't happen"

    def processEnded(self, reason):
        print "got procend with",reason
        self.proc = None
        if self.queued:
            self._spawn()
        self.queued = False
