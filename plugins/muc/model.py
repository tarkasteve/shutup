
import logging, copy
from common.utils import SignalEmitter
from common.model import JID, Conversation, ChatMessage

class MucJID(JID):
    passwd = None


# FIXME: This should be base-class in core
class MUCService(SignalEmitter):
    def __init__(self, name, type):
        SignalEmitter.__init__(self, ['activate'])
        self.name = name
        self.type = type
        self.udat = {}

    def activate(self):
        """We don't know what to do with this, but someone else may..."""
        self.emitsig('activate', service=self)

class MUCServiceList(dict, SignalEmitter):
    def __init__(self):
        SignalEmitter.__init__(self, ['add', 'remove', 'activate'])
        self.udat = {}

    # Override 'in' infix operator to give list-like behaviour
    def __iter__(self):
        return iter(self.values())

    def add(self, service):
        if service.name not in self.keys():
            self[service.name] = service
            self.emitsig('add', service=service)

    def remove(self, service):
        if service in self:
            list.remove(self, service)
            self.emitsig('remove', service=service)


class MUCMember(SignalEmitter):
    def __init__(self, jid, role, affil):
        self.jid = jid
        self.role = role
        self.affiliation = affil
        self.services = MUCServiceList()
        self.udat = {}

    def update(self, member):
        self.jid = member.jid
        self.role = member.role
        self.affiliation = member.affiliation

class MUCRoster(SignalEmitter):
    def __init__(self):
        SignalEmitter.__init__(self, ['add', 'remove', 'update'])
        self.members = {}

    def addUpdate(self, member):
        nick = member.jid.resource
        if nick in self.members:
            self.members[nick].update(member)
            member = self.members[nick]
            sig = 'update'
        else:
            self.members[nick] = member
            sig = 'add'

        self.emitsig(sig, member=member)

    def remove(self, member):
        nick = member.jid.resource
        if nick not in self.members:
            return
        self.emitsig('remove', member=self.members[nick])
        del self.members[nick]

    def findByNick(self, nick):
        if nick in self.members:
            return self.members[nick]
        else:
            return None


class MUCMessage(ChatMessage):
    def __init__(self, txt=None, sender=None, tx=False, sysmsg=False):
        ChatMessage.__init__(self, txt=txt, sender=sender, tx=tx)
        self.sysmsg = False

class MUCConversation(Conversation):
    type = 'groupchat'

    def __init__(self, account, contact, resource=None):
        Conversation.__init__(self, account, contact, resource)
        self.addSignal('subjectUpdate')

        self.roster = MUCRoster()
        self.services = MUCServiceList()

    # New attribs
    _subject = None
    def _setSubject(self, subject):
        self._subject = subject
        self.emitsig('subjectUpdate', subject=subject)
    def _getSubject(self):
        return self._subject
    subject = property(_getSubject, _setSubject)


    def addRxMsg(self, msg):
        # Override parent to handle loopbacks
        # FIXME: This causes any messages from myself that get added
        # from the history on connection to be expunged.
        if (msg.sender.resource == self.contact.jid.resource) and not msg.timestamp:
            msg.hidden = True
        if not msg.sender.resource:
            msg.sysmsg = True
        Conversation.addRxMsg(self, msg)

    def addSysMsg(self, msg):
        """Util for messages from the MUC room itself.  Msg is a string."""
        msg = MUCMessage(msg, sender=self.contact.jid, sysmsg=True)
        Conversation.addRxMsg(self, msg)


class MucInvite(object):
    def __init__(self, room, reason=''):
        self.room = room
        self.reason = reason

class MucInviteRX(MucInvite):
    def __init__(self, room, sender, reason='',):
        MucInvite.__init__(self, room, reason)
        self.sender = sender
    
    
    
