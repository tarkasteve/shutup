import gconf
import gtk
from common.model import Account
from common.utils import SignalEmitter


GLADEFILE = 'gnomeui/glade/jabber-gnome.glade'

PRESTYPES = (('offline', 'Offline'),
             ('online', 'Available'),
             ('away', 'Away'),
             ('xa', 'Extended Away'),
             ('dnd', 'Do Not Disturb'))

PRESCOLOR = {'offline':None,
             'online':'green',
             'away': 'yellow',
             'xa': 'blue',
             'dnd': 'red'}

PALETTE = ["#FF0000",
           "#0000FF",
           "#490302",
           "#444d99",
           "#e77e00",
           "#c58551",
           "#d8e7f1",
           "#d6f8c4",
           "#2c5c17"]

GCONF_PATH = '/apps/shutup'
def gpath(key):
    return GCONF_PATH + '/' + key


# FIXME: Just manually pulling for now, we should probably also listen for changes.
class GConfAccount(Account):
    def __init__(self):
        self.client = gconf.client_get_default()

        self._configured = self.client.get_bool(gpath("configured"))
        if self._configured:
            jidstr = self.client.get_string(gpath("jid"))
            passwd = self.client.get_string(gpath("passwd"))
            ssl = self.client.get_bool(gpath("ssl"))
            Account.__init__(self, jidstr, passwd, ssl=ssl)
        else:
            Account.__init__(self)

    _configured = False
    def _setconfigured(self, configured):
        self.client.set_bool(gpath("configured"), configured)
        self._configured = configured
    def _getconfigured(self): return self._configured
    configured = property(_getconfigured, _setconfigured)


    # Override the accessors for attributes we're interested in.    
    def _setjid(self, jid):
        if jid:
            jidstr = jid.full()
        else:
            jidstr = ''
        self.client.set_string(gpath("jid"), jidstr)
        self._jid = jid
    def _getjid(self): return self._jid
    jid = property(_getjid, _setjid)

    def _setpasswd(self, passwd):
        self.client.set_string(gpath("passwd"), passwd or "")
        self._passwd = passwd
    def _getpasswd(self): return self._passwd
    passwd = property(_getpasswd, _setpasswd)

    def _setssl(self, ssl):
        self.client.set_bool(gpath("ssl"), ssl)
        self._ssl = ssl
    def _getssl(self): return self._ssl
    ssl = property(_getssl, _setssl)
