
import pygtk
pygtk.require('2.0')
import gobject, gtk, gtk.glade, pango

from common.components import View, TrivialComponentPlugin, TrivialComponent


class TrivialGTKPlugin(TrivialComponentPlugin):
    """Base-class for the GTK aspect of plugins.  There will generally
       only be one instance of this per plugin, and it should be
       registered on plugin initialisation (see addComponentPlugin
       below).  Inheritors should emit a 'create' signal on creating a
       new component."""
    def __init__(self):
        TrivialComponentPlugin.__init__(self)
        # A list of items to be inserted into the menus, in order.
        # The format is a list of widgets (almost always GtkMenuItem or
        # GtkImageMenuItem).  See addComponentPlugin below for
        # implementation details.
        self.fileitems = []
        self.rosterctxitems = []
        self.rosterDefaultAction = None


class ComponentTab(gtk.HBox):
    """Tab object for a component view.  Usually for internal use
    only.  The parent component is passed in during initialisation."""
    def __init__(self, parent):
        gtk.HBox.__init__(self)
        # Create tab-handle
        img = gtk.Image()
        img.set_from_stock('gtk-close', 1)

        self.close = gtk.Button()
        self.close.add(img)
        self.close.set_relief(gtk.RELIEF_NONE)

        self.label = gtk.Label(parent.title)
        parent.addReceiver('titleUpdate', self.titleUpdate)

        self.pack_start(self.label)
        self.pack_start(self.close)
        self.show_all()

    def titleUpdate(self, component):
        self.label.set_text(component.title[0:15])


class TrivialGTKComponent(TrivialComponent):
    """View instance component.  Usually this will be
    per-'conversation', although a conversation may be other things
    such as an interactive game, etc."""

    def __init__(self, title):
        TrivialComponent.__init__(self, title)
        self.tab = ComponentTab(self)

