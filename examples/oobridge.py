import uno

# Get the uno component context from the PyUNO runtime
localContext = uno.getComponentContext()

# Create the UnoUrlResolver
resolver = localContext.ServiceManager.createInstanceWithContext(
				"com.sun.star.bridge.UnoUrlResolver", localContext )

# Connect to the running office
ctx = resolver.resolve( "uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext" )
smgr = ctx.ServiceManager

# Get the central desktop object
desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)

# Pull the current document, we asssume this is an Impress doc
doc = desktop.getCurrentComponent()
frame = desktop.getCurrentFrame()
ctrl = frame.getController()  # In impress this implements XDrawView
pages = doc.getDrawPages()    # Page list of the current impress document

# Set to some page ...
target = pages.getByIndex(8)
ctrl.setCurrentPage(target)
