
import logging
from twisted.internet import reactor
from muc.model import MUCConversation
from agmedia.base import MediaProcess
from vic import VicProcess
from rat import RatProcess
from common.components import TrivialComponentPlugin, PluginException

class AGMediaPlugin(TrivialComponentPlugin):
    name = "AG Media Handlers"
    author = "Steve Smith <ssmith@vislab.usyd.edu.au>"
    desc = "Adds Vic and Rat as handlers for chat-room multicast sessions."
    handle = "agmedia"

    def __init__(self, core):
        TrivialComponentPlugin.__init__(self)
        self.core = core


def init(app):
    global log
    log = logging.getLogger("process")

    if not app.findPlugin('muc'):
        log.info('No MUC plugin found, ag-media plugin aborting')
        return False
        
    global _vic, _rat
    _vic = VicProcess(app.account)
    _rat = RatProcess(app.account)

    log.info('Initialised ag-media plugin')
    return AGMediaPlugin(app)
