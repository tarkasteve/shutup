import gobject, gtk, gtk.glade, pango

class XFormDialog(gtk.Dialog):
    def __init__(self):
        gtk.Dialog.__init__(self, title='Dialog',
                            buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                     gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        # Setup
        self.set_border_width(12)
        self.vbox.set_spacing(6)

        # Title
        label = gtk.Label()
        label.set_markup('<b>Dialog</b>')
        self.vbox.pack_start(label)

        self.show_all()

        self.connect('response', self.response)
        self.connect('close', self.cancel)

        self.show_all()


    def response(self, dialog, response):
        self.hide_all()
        self.destroy()

    def cancel(self, dialog):
        self.destroy()


class BDialog(gtk.Dialog):
    def __init__(self):
        gtk.Dialog.__init__(self, title='Dialog',
                            buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                     gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        # Setup
        self.set_border_width(12)
        self.vbox.set_spacing(6)

        # Title
        b = gtk.Button(label="New")
        b.connect('clicked', self.doit)
        self.vbox.pack_start(b)

        self.show_all()

    def doit(self, widget):
        xf = XFormDialog()
        print xf


def main():
    BDialog()
    gtk.main()

if __name__ == '__main__':
    main()
