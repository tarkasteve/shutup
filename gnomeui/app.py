
import logging
import pygtk
pygtk.require('2.0')

import gobject, gtk, gtk.glade, pango
import gnome, gnome.ui
from twisted.internet import reactor

from common.model import JID, Conversation, Contact
from common.utils import SignalEmitter
from common.components import View
from gnomeui.chat import ChatView, ComponentTab, ChatTab
from gnomeui.config import GLADEFILE, PRESTYPES, PRESCOLOR


class TrivialComponent(SignalEmitter):
    """A trival component system.  Components should inherit this
    class and fill in the blanks, then register themselves with the
    main app ('view')"""
    def __init__(self):
        SignalEmitter.__init__(self, ['titleUpdate'])

    dat = None  # For container to store info

    _title = None
    def _setTitle(self, title):
        self._title = title
        self.emitsig('titleUpdate', component=self)
    def _getTitle(self):
        return self._title
    title = property(_getTitle, _setTitle)

    # Operations called on container events
    def close(self):
        raise NotImplementedError


class TrivialComponentPlugin(SignalEmitter):
    """Base-class for plugins utilising the trivial component system.
    This is essentially the factory for Trivial components.
    Inheritors should emit a 'create' signal on component creation."""
    def __init__(self):
        SignalEmitter.__init__(self, ['create'])

    # A list of items to be inserted into the file menu, in order.
    # The format is a list of widgets (almost always GtkMenuItem or
    # GtkImageMenuItem).  See addComponentPlugin below for
    # implementation details.
    fileitems = []


class GnomeApp(View):
    def getType(self):
        return 'gnome'

    def __init__(self, account, xmpp):
        self.xmpp = xmpp

        # Subscribe to updates to model
        self.account = account
        account.addReceiver('connectedUpdate', self.connectionState)
        account.addReceiver('gotAuthorised', self.gotAuthorised)
        account.roster.addReceiver('gotRoster', self.gotRoster)
        account.roster.addReceiver('gotPresence', self.gotContactPresence)
        account.conversations.addReceiver('create', self.newConversation)

        # Hook it up
        gnome.init("Jabber-Gnome", "0.1")
        self.wtree = gtk.glade.XML(GLADEFILE)

        # Pull out some useful widgets
        self.appbar = self.wtree.get_widget("appbar")
        self.presence = self.wtree.get_widget("presence")
        self.roster = self.wtree.get_widget("rostertree")
        self.tabs = self.wtree.get_widget("tabbook")
        self.accountdialog = self.wtree.get_widget("accountdialog")

        self.tabs.remove_page(0)  # Delete Glade setup page
        self.tabs.set_show_tabs(True)
        self.tabs.show_all()

        self.log = logging.getLogger("gui")

        self.roster.connect('button-press-event', self.roster_clicked)


    def run(self):
        ls = gtk.ListStore(gobject.TYPE_STRING,  # Long name
                           gobject.TYPE_STRING)  # Short name
        # FIXME: Get this from Jabber model
        for stat in PRESTYPES:
            ls.append([stat[1], stat[0]])
        self.presence.set_model(ls)
        self.presence.set_active(0)

        ts = gtk.TreeStore(gobject.TYPE_STRING,   # Name
                           gobject.TYPE_STRING,   # BGColor
                           gobject.TYPE_STRING,   # Font Weight
                           gobject.TYPE_STRING,   # Type (group, user, etc)
                           gobject.TYPE_PYOBJECT) # Contact object
        #ts.set_sort_func(1, self.compareIters)
        ts.set_sort_column_id(1, gtk.SORT_ASCENDING)
        self.roster.set_model(ts)

        col = gtk.TreeViewColumn("Contact", gtk.CellRendererText(),
                                 text=0, background=1)
        self.roster.append_column(col)
        
        # Done
        self.appbar.push("Initialised")
        self.appbar.set_progress_percentage(0)

        self.wtree.signal_autoconnect(self)

        if not self.account.configured:
            self.accountdialog.show_all()


    #################### GTK Signal Handlers ####################

    def on_connect_activated(self, widget):
        if not self.account.connected:
            self.presence.set_active(1)

    def roster_clicked(self, roster, event):
        if event.type == gtk.gdk._2BUTTON_PRESS:
            path, col, x, y = roster.get_path_at_pos(int(event.x), 
                                                     int(event.y))
            if not path:
                return

            # Lookup user data attached to this row
            model = roster.get_model()
            iter = model.get_iter(path)
            contact = model.get_value(iter, 4)
            if not contact:
                return

            # Get the conversation for this user.  If this doesn't
            # exist a 'newconversation' signal should result in its
            # creation.
            conv = self.account.conversations.getConversation(contact.jid)
            if not conv:
                conv = Conversation(self.account, contact)
                self.account.conversations.addConversation(conv)
                # Conversation creation triggers window creation via
                # 'newconversation' signal ...

                # FIXME: Pop something into the chat?

    def on_presence_changed(self, combo):
        ls = combo.get_model()
        presence = ls[combo.get_active()][1]
        # We implement connect by going online/available
        if presence != 'offline':
            if not self.account.connected:
                self.log.info("Connecting")
                self.xmpp.connect()
        #if presence == 'online':                
        #    presence = None
        self.account.presence = presence

    def quit(self, widget):
        self.xmpp.stop()

    # 'UI' interface callbacks for client
    def message(self, msg):
        self.appbar.push(msg)

    def passwd_cb(self, maxlen, verify, arg):
        self.appbar.push("Certificate passphrase requested")
        dia = self.wtree.get_widget("passdialog")
        text = self.wtree.get_widget("passtext")
        dia.show()
        dia.run()
        dia.hide()
        return text.get_text()    


    #################### Account Dialog ####################

    def on_accountedit_activate(self, widget):
        name = self.account.jid.user
        server = self.account.jid.host
        resource = self.account.jid.resource
        passwd = self.account.passwd
        ssl = self.account.ssl
        
        self.wtree.get_widget('accountname').set_text(name)
        self.wtree.get_widget('accountserver').set_text(server)
        self.wtree.get_widget('accountresource').set_text(resource)
        self.wtree.get_widget('accountpasswd').set_text(passwd)
        self.wtree.get_widget('accountssl').set_active(ssl)

        self.accountdialog.show_all()

    def on_accountok_clicked(self, widget):
        self.accountdialog.hide_all()

        name = self.wtree.get_widget('accountname').get_text()
        server = self.wtree.get_widget('accountserver').get_text()
        resource = self.wtree.get_widget('accountresource').get_text()
        passwd = self.wtree.get_widget('accountpasswd').get_text()
        ssl = self.wtree.get_widget('accountssl').get_active()

        self.account.jid = JID(tuple=(name, server, resource))
        self.account.passwd = passwd
        self.account.ssl = ssl
        self.account.configured = True
        
    def on_accountcancel_clicked(self, widget):
        self.accountdialog.hide_all()


    #################### Component Handler ####################

    def addComponentPlugin(self, plugin):
        # Lookup last position in file menu by finding the plugin
        # separator.
        # FIXME: I'm sure there's a better way to do this
        menu = self.wtree.get_widget('file1_menu')
        ppos = 0
        for i in menu.get_children():
            if i.get_name() == 'plugend':
                break
            ppos += 1
        for i in plugin.fileitems:
            menu.insert(i, ppos)
            ppos += 1
        menu.show_all()

        # We want to know about new components ...
        plugin.addReceiver('create', self.newComponent)

    def newComponent(self, component):
        self.log.info("Received component init signal for "+component.title)

        # FIXME: We use tabs as containers for components for now,
        # this may become configurable later.
        tab = ComponentTab(component.title)
        tab.close.connect('clicked', self.tabclose, component)
        self.tabs.append_page(component, tab_label=tab)

        component.dat = tab
        component.addReceiver('titleUpdate', self.titleUpdate)

    def titleUpdate(self, component):
        label = component.dat.label
        label.set_text(component.title[0:15])

    def tabclose(self, widget, chat):
        page = self.tabs.page_num(chat)
        self.tabs.remove_page(page)
        chat.close()


    #################### Model Handlers ####################

    def connectionState(self, connected):
        if not connected:
            self.message("Disconnected")
            self.presence.set_active(0)

    def gotAuthorised(self, authed):
        self.message("Authorisation passed")

    def gotError(self, error):
        self.log.info("GUI got error")

    def gotContactPresence(self, user):
        self.log.info("GUI got presence for user "+user.name+'='+user.presence)
        if user.presence not in PRESCOLOR:
            return
        ts = self.roster.get_model()
        ref = user.udat
        row = ts.get_iter(ref.get_path())
        ts.set_value(row, 1, PRESCOLOR[user.presence])
        
    def gotRoster(self, roster):
        self.log.info("GUI got roster")
        ts = self.roster.get_model()
        groups = roster.groups()
        for gname in groups.keys():
            giter = ts.append(None, (gname or 'Ungrouped', None, pango.WEIGHT_BOLD, 'group', None))
            for contact in groups[gname]:
                row = ts.append(giter, (contact.name, None, pango.WEIGHT_NORMAL, 'contact', contact))
                # Store ref to created row with model
                ref = gtk.TreeRowReference(ts, ts.get_path(row))
                contact.udat = ref
        self.roster.expand_all()

    def newConversation(self, conv):
        self.log.info("Received conversation init signal")
        if conv.type != 'chat':
            return

        chat = ChatView(conv)
        tab = ChatTab(conv)

        self.tabs.append_page(chat, tab_label=tab)

        tab.close.connect('clicked', self.tabclose, chat)

