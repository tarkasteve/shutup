
from agmedia.base import MediaProcess

class VicProcess(MediaProcess):
    def __init__(self, account):
        MediaProcess.__init__(self, account)
        self.path = 'vic'

    def serviceAdd(self, service):
        self.log.info("Vic received service add")
        if service.media == 'video':
            service.addReceiver('activate', self.activate)

    def genParams(self):
        parms = ['-t '+self.service.ttl, self.service.addr+'/'+self.service.port]
        if self.service.enctype:
            parms.append('-K '+self.service.enckey)
        return [self.path] + parms
