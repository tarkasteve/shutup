
import sys, os
import logging

from common.utils import SignalEmitter

class PluginException(Exception):
    pass

class TrivialComponent(SignalEmitter):
    """A trival component system.  Components should inherit this
    class and fill in the blanks, then register themselves with the
    main app ('view')"""
    def __init__(self, title):
        SignalEmitter.__init__(self, ['titleUpdate'])
        self._title = title  # Skip signal on initialisation

    dat = None  # For container to store info

    _title = None
    def _setTitle(self, title):
        self._title = title
        self.emitsig('titleUpdate', component=self)
    def _getTitle(self):
        return self._title
    title = property(_getTitle, _setTitle)

    # Operations called on container events
    def close(self):
        pass


class TrivialComponentPlugin(SignalEmitter):
    """Base-class for plugins utilising the trivial component system.
    An instance of this should be returned by plugin initialisation
    routines.  This is essentially the factory for Trivial components.
    Inheritors should emit a 'create' signal on component creation."""
    name = None
    author = None
    path = None
    desc = None
    handle = None  # Short name for lookups

    def __init__(self):
        SignalEmitter.__init__(self, ['create'])


class View(object):
    def getType(self):
        # Subclasses should return a string designating their UI type
        # (gnome, wx, etc.)
        raise NotImplementedError

# Base application class
class Application(object):
    account = None
    view = None
    xmpp = None

    _plugins = {}
    def loadPlugins(self, pdir='plugins'):
        #sys.path.append(pdir)
        for path, dirs, files in os.walk(pdir):
            if '__init__.py' in files:
                elems = path.split('/')
                #modname = elems[len(elems)-1]
                #mod = __import__('.'.join(elems[1:len(elems)]))
                modname = '.'.join(elems)

                # Import the found module. Note that the last
                # parameter is important otherwise only the base
                # modules (ie. 'plugins') is imported.  This appears
                # to be by design.
                mod = __import__(modname, globals(), locals(), [''])
                if 'init' in dir(mod):
                    plugin = mod.init(self)
                    if plugin:
                        self._plugins[modname] = plugin

    def pluginList(self):
        return self._plugins.values()

    def findPlugin(self, handle):
        """Retrieve plugin by handle"""
        for plugin in self.pluginList():
            if plugin.handle == handle:
                return plugin
        return False

    def run(self):
        raise NotImplementedError
