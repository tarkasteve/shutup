
import logging
import pygtk
pygtk.require('2.0')

import gobject, gtk, gtk.glade, pango
from twisted.internet import reactor

from cell_renderer_image import CellRendererImage

from common.model import JID, Conversation, Contact
from common.utils import SignalEmitter
from common.components import View, TrivialComponentPlugin, TrivialComponent
import gtkui
from gtkui.config import getWidgets, PRESTYPES, PRESCOLOR, getIcons
from gtkui.components import TrivialGTKComponent, TrivialGTKPlugin
from gtkui.roster import GtkRoster



class GTKView(View):
    def getType(self):
        return 'gtk'

    def __init__(self, core):
        self.core = core
        self.xmpp = core.xmpp
        self.account = core.account
        self.plugins = []

        self.log = logging.getLogger("gui")

        # Subscribe to updates to model
        self.account.addReceiver('connectedUpdate', self.connectionState)
        self.account.addReceiver('gotAuthorised', self.gotAuthorised)

        # Hook it up
        #gtk.init("ShutUp", "0.1")
        self.wtree = getWidgets()

        # Pull out some useful widgets
        self.statusbar = self.wtree.get_widget("statusbar")
        self.presence = self.wtree.get_widget("presence")
        self.tabs = self.wtree.get_widget("tabbook")
        self.accountdialog = self.wtree.get_widget("accountdialog")

        self.rosterui = GtkRoster(core)
        self.wtree.get_widget("rosterscrolledwindow").add(self.rosterui)
        self.rosterui.show_all()

        self.tabs.remove_page(0)
        self.tabs.set_show_tabs(True)
        self.tabs.show_all()


    def run(self):
        # Setup presence selector
        ls = gtk.ListStore(gobject.TYPE_STRING,  # Long name
                           gobject.TYPE_STRING,  # Short name
                           gtk.Image)            # Icon
        # FIXME: Get this from Jabber model
        icons = getIcons()
        for stat in PRESTYPES:
            short = stat[0]
            ls.append([stat[1], short, icons[short]])
        self.presence.clear()
        cell = CellRendererImage()
        self.presence.pack_start(cell, False)
        self.presence.add_attribute(cell, 'image', 2)
        cell = gtk.CellRendererText()
        cell.set_property('xpad', 5) # padding for status text
        self.presence.pack_start(cell, False)
        self.presence.add_attribute(cell, 'text', 0)
        self.presence.set_model(ls)
        self.presence.set_active(0)

        # Roster
        self.rosterui.reset()

        # Plugin view
        view = self.wtree.get_widget("pluginslistview")
        col = gtk.TreeViewColumn("Plugins", gtk.CellRendererText(),
                                 text=0)
        view.append_column(col)
        sel = view.get_selection()
        sel.connect('changed', self.on_pluginselection_changed)

        # Done
        self.message("Initialised")

        self.wtree.signal_autoconnect(self)

        if not self.account.configured:
            self.accountdialog.show_all()


    #################### GTK Signal Handlers ####################

    def on_connect_activated(self, widget):
        if not self.account.connected:
            self.presence.set_active(1)

    def on_presence_changed(self, combo):
        ls = combo.get_model()
        presence = ls[combo.get_active()][1]
        # We implement connect by going online/available
        if presence != 'offline':
            if not self.account.connected:
                self.log.info("Connecting")
                self.xmpp.connect()
        #if presence == 'online':                
        #    presence = None
        self.account.presence = presence

    def quit(self, widget, event=None):
        self.xmpp.stop()

    # 'UI' interface callbacks for client
    def message(self, msg):
        ctx = self.statusbar.get_context_id('General')
        self.statusbar.push(ctx, msg)

    def passwd_cb(self, maxlen, verify, arg):
        self.message("Certificate passphrase requested")
        dia = self.wtree.get_widget("passdialog")
        text = self.wtree.get_widget("passtext")
        dia.show()
        dia.run()
        dia.hide()
        return text.get_text()    


    #################### Plugins Dialog ####################

    def on_plugins_activate(self, widget):
        view = self.wtree.get_widget("pluginslistview")

        ls = view.get_model()
        if not ls:
            ls = gtk.ListStore(gobject.TYPE_STRING,   # Name
                               gobject.TYPE_PYOBJECT) # Plugin obj
            for p in self.core.pluginList():
                ls.append([p.name, p])
            view.set_model(ls)

        self.wtree.get_widget("pluginsdialog").show_all()

    def on_pluginselection_changed(self, sel):
        model, paths = sel.get_selected_rows()
        if not paths:
            return

        iter = model.get_iter(paths[0])
        plugin = model.get_value(iter, 1)
        
        self.wtree.get_widget('plugnamelabel').set_text(plugin.name)
        self.wtree.get_widget('plugauthorlabel').set_text(plugin.author)
        self.wtree.get_widget('plugpathlabel').set_text(str(plugin.path))
        self.wtree.get_widget('plugdesclabel').set_text(str(plugin.desc))
        self.wtree.get_widget('plugdesclabel').set_line_wrap(True)

    def on_pluginsok_clicked(self, widget):
        self.wtree.get_widget("pluginsdialog").hide_all()


    #################### Account Dialog ####################

    def on_accountedit_activate(self, widget):
        name = self.account.jid.user
        server = self.account.jid.host
        resource = self.account.jid.resource
        passwd = self.account.passwd
        ssl = self.account.ssl
        
        self.wtree.get_widget('accountname').set_text(name)
        self.wtree.get_widget('accountserver').set_text(server)
        self.wtree.get_widget('accountresource').set_text(resource)
        self.wtree.get_widget('accountpasswd').set_text(passwd)
        self.wtree.get_widget('accountssl').set_active(ssl)

        self.accountdialog.show_all()

    def on_accountok_clicked(self, widget):
        self.accountdialog.hide_all()

        name = self.wtree.get_widget('accountname').get_text()
        server = self.wtree.get_widget('accountserver').get_text()
        resource = self.wtree.get_widget('accountresource').get_text()
        passwd = self.wtree.get_widget('accountpasswd').get_text()
        ssl = self.wtree.get_widget('accountssl').get_active()

        self.account.jid = JID(tuple=(name, server, resource))
        self.account.passwd = passwd
        self.account.ssl = ssl
        self.account.configured = True
        self.account.flush()
        
    def on_accountcancel_clicked(self, widget):
        self.accountdialog.hide_all()

        
    #################### Component Handler ####################

    def addComponentPlugin(self, plugin):
        self.plugins.append(plugin)

        # Lookup last position in file menu by finding the plugin
        # separator.
        # FIXME: I'm sure there's a better way to do this
        menu = self.wtree.get_widget('filemenu').get_submenu()
        ppos = 0
        for i in menu.get_children():
            if i.get_name() == 'plugend':
                break
            ppos += 1
        for i in plugin.fileitems:
            menu.insert(i, ppos)
            ppos += 1
        menu.show_all()

        # Default action
        if plugin.rosterDefaultAction:
            if self.rosterui.defaultAction:
                self.log.warning("Roster default action being overwritten")
            self.rosterui.defaultAction = plugin.rosterDefaultAction

        # We want to know about new components ...
        plugin.addReceiver('create', self.newGtkComponent)

    def newGtkComponent(self, component):
        self.log.info("Received component init signal for "+component.title)

        # FIXME: We use tabs as containers for components for now,
        # this may become configurable later.
        self.tabs.append_page(component, tab_label=component.tab)
        component.tab.close.connect('clicked', self.tabclose, component)

    def tabclose(self, widget, component):
        page = self.tabs.page_num(component)
        self.tabs.remove_page(page)
        component.close()


    #################### Model Handlers ####################

    def connectionState(self, connected):
        if not connected:
            self.message("Disconnected")
            self.presence.set_active(0)
            self.rosterui.reset()

    def gotAuthorised(self, authed):
        self.message("Authorisation passed")

    def gotError(self, error):
        self.log.info("GUI got error")
