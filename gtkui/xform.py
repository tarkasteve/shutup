
import gobject, gtk, gtk.glade, pango
from common.model import XOption, XField, XDataForm

class XFormDialog(gtk.Dialog):
    def __init__(self, form, cb=None, title='Data-Form Dialog'):
        gtk.Dialog.__init__(self, title=title,
                            buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                     gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        self.targetCB = cb
        self.form = form

        # Setup
        self.set_border_width(12)
        self.vbox.set_spacing(6)

        # Title
        label = gtk.Label()
        label.set_markup('<b>%s</b>'% (form.title or title))
        self.vbox.pack_start(label)

        self.table = gtk.Table(rows=len(form.fields), columns=2)
        self.vbox.pack_start(self.table)

        row = 1
        for field in form.fields:
            label = gtk.Label(str=field.label)
            label.set_alignment(0.0, 0.5)
            label.set_justify(gtk.JUSTIFY_LEFT)

            def sdecode(widget):
                return widget.get_text()

            def bdecode(widget):
                if widget.get_active():
                    return 1
                else:
                    return 0

            if field.type == 'boolean':
                val = gtk.ToggleButton()
                val.set_active(field.value)
                field.retriever = bdecode

            elif field.type == 'text-single':
                val = gtk.Entry()
                if field.value:
                    val.set_text(field.value)
                field.retriever = sdecode

            elif field.type == 'text-private':
                val = gtk.Entry()
                val.set_visibility(False)
                if field.value:
                    val.set_text(field.value)
                field.retriever = sdecode

            else:  # FIXME: More needed
                val = gtk.Label(str='Unhandled')
            field.widget = val

            self.table.set_row_spacing(row, 6)
            self.table.set_col_spacing(0, 6)
            self.table.set_col_spacing(1, 6)
            self.table.attach(label, 0, 1, row, row+1)
            self.table.attach(val, 1, 2, row, row+1, xoptions=gtk.SHRINK)
            row += 1
        
        self.connect('response', self.response)
        self.connect('close', self.cancel)

        self.show_all()

    def response(self, dialog, response):
        self.hide_all()

        if response != gtk.RESPONSE_ACCEPT:
            self.cancel(dialog)
            return

        # Extract
        for field in self.form.fields:
            if field.retriever:
                field.value = field.retriever(field.widget)

        self.targetCB(self.form)
        self.destroy()

    def cancel(self, dialog):
        self.targetCB(self.form, cancelled=True)
        self.destroy()
