#
# XMPP 'Adventure' client.
#
import sys, logging, re, datetime, pytz
from string import split, rstrip
from twisted.internet import reactor, ssl
from twisted.words.xish import xpath
from twisted.words.protocols.jabber import xmlstream, client
from twisted.words.xish.domish import Element
from common.model import Roster, JID, ChatMessage, Conversation, Contact
from common.utils import SignalEmitter

SASLAUTHMECH = 'DIGEST-MD5'

#NODEPATH = 'home/moth.vislab.usyd.edu.au/tarka/test-node'
NODEPATH = 'pubsub/nodes/test-node'

XNS_CLIENT = 'jabber:client'
XNS_ROSTER = 'jabber:iq:roster'
XNS_XDATA = 'jabber:x:data'
XNS_XDELAY = 'jabber:x:delay'
XNS_PRIVATE = 'jabber:iq:private'
XNS_PUBSUB = "http://jabber.org/protocol/pubsub"
XNS_DISCO = "http://jabber.org/protocol/disco"
XNS_DISCOINFO = XNS_DISCO + "#info"
XNS_DISCOITEMS = XNS_DISCO + "#items"

LOGNAME = 'xmpp'


########## Domish modifications ##########

class Stanza(Element):
    def __init__(self, qname, defaultUri = None, attribs = None):
        Element.__init__(self, qname, defaultUri=defaultUri,
                         attribs=attribs)

    def addStanza(self, name, xns=None, content=None, attribs=None):
        xns = xns or self.defaultUri
        e = self.addChild(Stanza((xns, name)))
        if attribs:
            for k in attribs.keys():
                e[k] = attribs[k]
        if content:
            e.addContent(content)
        return e

class Presence(Stanza):
    def __init__(self, show=None, type=None):
        if type:
            attribs = {'type':type}
        else:
            attribs = None
        Stanza.__init__(self, (XNS_CLIENT, "presence"), attribs=attribs)
        if show:
            self.addStanza('show', xns=XNS_CLIENT, content=show)

class IQ(Stanza):
    def __init__(self, type, attr):
        Stanza.__init__(self, (XNS_CLIENT, "iq"), attribs=attr)
        if 'id' not in self.attributes:
            self.addUniqueId()
        self["type"] = type


class XMessageMix(object):
    # JEP-0091: "CCYYMMDDThh:mm:ss"
    _tsre = re.compile('(\d\d)(\d\d)(\d\d)(\d\d)T(\d\d):(\d\d):(\d\d)')
    def __init__(self, xml):
        if 'from' in xml.attributes:
            self.sender = JID(xml.attributes['from'])
        else:
            # FIXME: Is this an error?  XML schema says optional, but
            # I'm not sure this makes sense
            self.sender = None

        body = xpath.queryForNodes('/message/body', xml)
        if body:
            self.txt = str(body[0])

        ts = xpath.queryForNodes("//x[@xmlns='%s']" % XNS_XDELAY, xml)
        if ts and 'stamp' in ts[0].attributes:
            ts = ts[0].attributes['stamp']
            match = self._tsre.search(ts)
            if not match:
                logging.getLogger(LOGNAME).warn("Invalid timestamp in message")
                ts = None
            else:
                (c,y,mon,d,h,min,s) = match.groups()
                ts = datetime.datetime(int(c+y), int(mon), int(d),
                                       int(h), int(min), int(s), tzinfo=pytz.utc)
        else:
            ts = None

        self.timestamp = ts


#################### Common message decoder utils #################### 

def getMsgType(xml):
    # RFC3921: ... if an application receives a message with no
    # 'type' attribute or the application does not understand the
    # value of the 'type' attribute provided, it MUST consider the
    # message to be of type "normal" ...
    if 'type' in xml.attributes:
        type = xml.attributes['type']
    else:
        type = 'normal'
    return type


########## Protocol Handlers ##########

class XMPPHandler(SignalEmitter):
    # See http://www.jabber.org/registrar/disco-categories.html
    identity = {'category':'client',
                'type':'pc'}  # Default, override if inheriting for bots etc.

    # Features supported, returned by Disco
    # queries.  Can be added-to by plugins, see below.
    features = []

    def __init__(self, acct):
        SignalEmitter.__init__(self, ['iq', 'presence', 'message',
                                      'chatmsg', 'mucmsg', 'errormsg', 'headlinemsg'])

        self.account = acct
        self.stream = None

        # State
        self.endsent = False
        self.quitting = False

        self.log = logging.getLogger(LOGNAME)
        self.xlog = logging.getLogger("xstream")


    def addFeature(self, xns):
        if xns not in self.features:
            self.features.append(xns)

    _iqHandles = {}
    def IQHandle(self, iq, cb, **kwargs):
        self._iqHandles[iq['id']] = (cb, kwargs)

    def callIQHandle(self, iq):
        if iq['id'] in self._iqHandles:
            (cb, kwargs) = self._iqHandles[iq['id']]
            cb(iq, **kwargs)  # Call-it
            del self._iqHandles[iq['id']]
            return True
        else:
            return False


    def _xrx(self,xml):
        self.xlog.debug('RX: '+xml)

    def _xtx(self,xml):
        self.xlog.debug("TX:"+xml)

    def connect(self, retry=0):
        self.log.info("Connection initiated")
        # Initialise connection and authorisation

        factory = client.basicClientFactory(self.account.jid, self.account.passwd)

        factory.addBootstrap(xmlstream.STREAM_CONNECTED_EVENT, self.connected)
        factory.addBootstrap(xmlstream.STREAM_AUTHD_EVENT, self.authorised)
        factory.addBootstrap(xmlstream.STREAM_ERROR_EVENT, self.error)
        factory.addBootstrap(xmlstream.STREAM_END_EVENT, self.gotEnd)

        if self.account.ssl:
            reactor.connectSSL(self.account.jid.host, 5223, # FIXME: Configure port
                               factory, ssl.ClientContextFactory())
        else:
            reactor.connectTCP(self.account.jid.host, 5222, factory)
    
    def connected(self, stream):
        self.stream = stream
        # Debug IO
        stream.rawDataInFn = self._xrx
        stream.rawDataOutFn = self._xtx

        self.account.connected = True

    def stop(self):
        if self.stream:
            self.quitting = True
            self.endstream()
        else:
            reactor.stop()

    def send(self, xml):
        self.stream.send(xml)

    def endstream(self):
        self.sendPresence(None, type='unavailable') # RFC-3921, 5.1.5
        self.send('</stream:stream>')
        self.endsent = True

    def gotEnd(self, stream):
        self.log.info("Received end of stream from server")
        if not self.endsent:
            self.endstream()

        if self.quitting:
            reactor.stop()
        else:
            self.account.authorised = False
            self.account.connected = False

    def authorised(self, stream):

        stream.addObserver("/message",  self.gotMessage)
        stream.addObserver("/presence", self.gotPresence)
        stream.addObserver("/iq",       self.gotIQ)

        self.account.authorised = True

        # RFC3921: "After establishing a session, a client SHOULD send
        # initial presence and request its roster as described below,
        # although these actions are OPTIONAL."
        self.reqRoster()
        self.sendPresence(self.account.presence)

        # Further presence updates will be triggered by model:
        self.account.addReceiver('presenceUpdate', self.sendPresence)
                

    ##
    # Error interpreter
    # Errors from RFC3920: <bad-format/>
    #                      <bad-namespace-prefix/>
    #                      <conflict/>
    #                      <connection-timeout/>
    #                      <host-gone/>
    #                      <host-unknown/>
    #                      <improper-addressing/>
    #                      <internal-server-error/>
    #                      <invalid-from/>
    #                      <invalid-id/>
    #                      <invalid-namespace/>
    #                      <invalid-xml/>
    #                      <not-authorized/>
    #                      <policy-violation/>
    #                      <remote-connection-failed/>
    #                      <resource-constraint/>
    #                      <restricted-xml/>
    #                      <see-other-host/>
    #                      <system-shutdown/>
    #                      <undefined-condition/>
    #                      <unsupported-encoding/>
    #                      <unsupported-stanza-type/>
    #                      <unsupported-version/>
    #                      <xml-not-well-formed/>
    def error(self,err):
        #show = xpath.queryForNodes('/presence/show', iq)        
        pass

    def gotIQ(self,iq):
        if 'id' not in iq.attributes:
            return # FIXME: Buggy clients, or is this valid?
        
        self.log.info("IQ id=%s" % iq['id'])

        # Check for errors and handle (CORE RFC, 9.3)
        type = iq['type']
        if type == 'error':
            # FIXME: Handle errors
            for err in xpath.queryForNodes('/iq/error', iq):
                self.log.warning("Got error: "+err.toXml().encode('utf8'))

        if not self.callIQHandle(iq):
            self.processIQ(iq)

        self.emitsig('iq', iq=iq)


    def processIQ(self, iq):
        """Process IQ initiated by a remove entity"""
        self.log.info("Got unknown IQ: "+iq.toXml().encode('utf8'))
        type = iq.attributes['type']
        q = xpath.queryForNodes('/iq/query', iq)
        if not q:
            return # FIXME: More needed?

        q = q[0]
        if q.uri == XNS_DISCOINFO and type == 'get':
            self.log.info("Process disco#info request")
            rep = IQ('result', {'to':iq.attributes['from'],
                                'id':iq.attributes['id']})
            qrep = rep.addStanza('query', xns=XNS_DISCOINFO)
            qrep.addStanza('identity', attribs=self.identity)
            for f in self.features:
                qrep.addStanza('feature', attribs={'var':f})
            self.send(rep)


    #################### Message Send/Receive ####################

    def gotPresence(self,pres):
        self.log.info("Got presence from %s: "+pres['from'])
        if 'type' in pres.attributes:
            type = pres.attributes['type']
        else:
            type = None
        jid = JID(pres['from'])

        # See RFC3921, 2.2
        if type == 'unavailable':
            self.account.roster.setPresence(jid, 'offline')

        elif type == None:
            show = xpath.queryForNodes('/presence/show', pres)
            if show:
                p = str(show[0])
            else:
                p = 'online'
            self.account.roster.setPresence(jid, p)

        # RFC3921, 2.2.1
        # FIXME: Implement
        elif type == 'subscribe':
            pass
        elif type == 'subscribed':
            pass
        elif type == 'unsubscribe':
            pass
        elif type == 'unsubscribed':
            pass
        elif type == 'probe':
            pass
        elif type == 'error':
            pass
        else:
            assert(None)   # FIXME: Exception?

        self.emitsig('presence', sender=jid, pres=pres)


    def sendPresence(self, presence, type=None):
        if presence == 'online' or presence == 'available':
            presence = None
        presence = Presence(presence, type=type)
        self.send(presence)


    #################### Message Send/Receive ####################

    def gotMessage(self,msg):
        self.log.debug("MESSAGE: %s" % msg.toXml())

        type = getMsgType(msg)
        msg.attributes['type'] = type  # In-case it wasn't set

        if 'from' in msg.attributes:
            sender = JID(msg.attributes['from'])
        else:
            # FIXME: Is this an error?  XML schema says optional, but
            # I'm not sure this makes sense
            sender = None

        # See RFC3921 2.1.1
        if type == 'chat':
            self.emitsig('chatmsg', msg=msg)

        elif type == 'groupchat':
            self.emitsig('mucmsg', msg=msg)

        elif type == 'error':
            self.emitsig('errormsg', msg=msg)

        elif type == 'headline':
            self.emitsig('headlinemsg', msg=msg)

        else:  # i.e. 'normal' or other
            self.emitsig('message', msg=msg)


    #################### Roster ####################

    def reqRoster(self):
        iq = IQ('get', None)
        iq.addStanza('query', xns=XNS_ROSTER)

        self.IQHandle(iq, self.rosterCB)

        self.send(iq)

    def rosterCB(self, iq):
        ql = xpath.queryForNodes("/iq/query", iq)
        q = ql[0]
        
        # Pull items, 'decode', and place in their groups
        items = xpath.queryForNodes("/query/item", q)
        for elem in items:
            # Pull out known attributes
            if 'name' in elem.attributes:
                name = elem.attributes['name']
            else:
                name = None
            if 'subscription' in elem.attributes:
                sub = elem.attributes['subscription']
            else:
                sub = None
            jid = JID(elem.attributes['jid'])


            contact = self.account.roster.getContact(jid)
            if not contact:
                contact = Contact(jid)                
            contact.name = name
            contact.sub = sub

            grp = xpath.queryForNodes("/item/group", elem)
            if grp:
                contact.group = str(grp[0])

            self.account.roster.setContact(contact)


    #################### PubSub ####################

    def createNode(self, path):
        ps = Stanza((XNS_PUBSUB,"pubsub"))
        ps.addStanza("create", attribs={'node':path})

        iq = IQ('set', {'to':'pubsub.'+self.account.jid.host,
                        'from':self.account.jid.userhost()})
        iq.addChild(ps)
        self.send(iq)

    def setItem(self, path, item, content):
        iq = IQ('set', {'to':'pubsub.'+self.account.jid.host,
                        'from':self.account.jid.full()})

        ps = Stanza((XNS_PUBSUB,"pubsub"))
        e = ps.addStanza("publish", attribs={'node':path})
        e.addStanza("item", attribs={'id':item}, content=content)
        iq.addChild(ps)
        self.send(iq)


    def getItem(self, host, path, item, target=None):
        iq = IQ('get', {'to':host, 'from':self.account.jid.full()})
        ps = iq.addStanza('pubsub',xns=XNS_PUBSUB)
        items = ps.addStanza("items", attribs={'node':path})
        items.addStanza("item", attribs={'id':item})

        self.IQHandle(iq, self.gotItem, item=item, target=target)
        self.send(iq)

    def gotItem(self, iq, item, target=None):
        self.log.info("Got item IQ, looking for item %s" % item)
        xp = "//item[@id='%s']" % item
        items = xpath.queryForNodes(xp, iq)
        content = str(items[0])
        self.log.info("Got item value: "+content)
        if target:
            target(content)

    def getNode(self, path):
        iq = IQ('get', {'to':'pubsub.'+self.account.jid.host,
                        'from':self.account.jid.full()})
        ps = Stanza((XNS_PUBSUB,"pubsub"))
        ps.addStanza("items", attribs={'node':path})
        iq.addChild(ps)

        self.IQHandle(iq, self.gotNode)
        self.send(iq)

    def gotNode(self, obj):
        self.log.info("Got node value: "+obj.toXml())

    def subNode(self, path, userec=False):
        if userec:
            jid = self.account.jid.full()
        else:
            jid = self.account.jid.userhost()

        iq = IQ('set', {'to':'pubsub.'+self.account.jid.host,
                        'from':jid})
        ps = Stanza((XNS_PUBSUB,"pubsub"))
        
        ps.addStanza("subscribe", attribs={'node':path,
                                           'jid':jid})

        iq.addChild(ps)
        self.send(iq)

    def getSubs(self):
        iq = IQ('get', {'to':'pubsub.'+self.account.jid.host,
                        'from':self.account.jid.full()})
        ps = Stanza((XNS_PUBSUB,"pubsub"))
        ps.addStanza("affiliations")
        iq.addChild(ps)
        self.send(iq)

    def unsub(self, userec=False):
        if userec:
            jid = self.account.jid.full()
        else:
            jid = self.account.jid.userhost()

        iq = IQ('set', {'to':'pubsub.'+self.account.jid.host,
                        'from':jid})
        ps = Stanza((XNS_PUBSUB,"pubsub"))
        ps.addStanza("unsubscribe", attribs={'node':NODEPATH,
                                             'jid':jid})
        iq.addChild(ps)
        self.send(iq)

    def delNode(self, path):
        iq = IQ('set', {'to':'pubsub.'+self.account.jid.host,
                        'from':self.account.jid.userhost()})
        ps = iq.addStanza('pubsub', xns=XNS_PUBSUB)
        ps.addStanza('delete', attribs={'node':path})

        self.send(iq)
        

    #################### Disco ####################

    def queryDiscoItems(self, to, node=None, cb=None):
        iq = IQ('get', {'to':to, 'from':self.account.jid.full()})
        if node:
            iq.attributes['node'] = node
        iq.addStanza('query', xns=XNS_DISCOITEMS)

        if cb:
            self.IQHandle(iq, cb)

        self.send(iq)

    def queryDiscoInfo(self, to, node=None, cb=None):
        iq = IQ('get', {'to':to, 'from':self.account.jid.full()})
        iq.addStanza('query', xns=XNS_DISCOINFO)

        if cb:
            self.IQHandle(iq, cb)

        self.send(iq)

