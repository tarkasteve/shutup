
import logging, copy
from twisted.words.protocols.jabber import jid
from common.utils import SignalEmitter

class JID(jid.JID):
    pass


class Account(SignalEmitter):
    def __init__(self, jidstr=None, passwd=None, ssl=False):
        SignalEmitter.__init__(self, ['gotAuthorised', 'presenceUpdate',
                                      'connectedUpdate'])

        if jidstr:
            self.jid = JID(jidstr)
        else:
            self.jid = None
        self.passwd = passwd
        self.ssl = ssl

        self.roster = Roster(self)
        self.conversations = Conversations(self)

    _connected = False
    def _setConnected(self, connected):
        self._connected = connected
        self.emitsig('connectedUpdate', connected=connected)
    def _getConnected(self):
        return self._connected
    connected = property(_getConnected, _setConnected)

    _authorised = False
    def _setAuthorised(self, authed):
        self._authorised = authed
        self.emitsig('gotAuthorised', authed=authed)
    def _getAuthorised(self):
        return self._authorised
    authorised = property(_getAuthorised, _setAuthorised)

    _presence = "offline"
    def _setPresence(self, presence):
        self._presence = presence
        self.emitsig('presenceUpdate', presence=presence)
    def _getPresence(self):
        return self._presence
    presence = property(_getPresence, _setPresence)


class Contact(object):
    def __init__(self, jid, name=None, sub=None):
        self.jid = jid
        self._name = name
        self.resource = None
        self.presence = 'offline'
        self.sub = sub
        self.udat = None
        self.group = None
        
    def _getName(self):
        if self._name == None:
            return self.jid.userhost()
        else:
            return self._name
    def _setName(self, name):
        self._name = name
    name = property(_getName, _setName)

class InvalidUpdate(Exception):
    pass

class Roster(SignalEmitter):
    def __init__(self, account):
        SignalEmitter.__init__(self, ['gotContact', 'updateContact', 'gotPresence'])
        
        # Maintain users in grouped and ungrouped hashes
        self.account = account
        self._contacts = {}

    def groups(self):
        """Returns hash of group-names containing a list of contacts"""
        #return self._groups.values()
        h = {}
        for c in self._contacts.values():
            if c.group not in h:
                h[c.group] = []
            h[c.group].append(c)
        return h

    def getContact(self, jid):
        if jid.userhost() in self._contacts:
            return self._contacts[jid.userhost()]
        else:
            return None

    def setContact(self, contact):
        jid = contact.jid
        if jid.userhost() in self._contacts:
            if self._contacts[jid.userhost()] != contact:
                raise InvalidUpdate('Roster updated with new contact')
            self.emitsig('updateContact', contact=contact)
        else:
            self._contacts[jid.userhost()] = contact
            self.emitsig('gotContact', contact=contact)
            

    def setPresence(self, jid, pres):
        contact = self.getContact(jid)
        if contact:
            contact.presence = pres
            self.emitsig('gotPresence', user=contact)


class Message(object):
    def __init__(self, txt=None, sender=None, tx=False):
        self.txt = txt
        self.tx = tx
        self.sender = sender
        self.timestamp = None

# FIXME: Can these be moved to the Chat plugin?
class ChatMessage(Message):
    hidden = False

class Conversation(SignalEmitter):
    type = 'chat'

    def __init__(self, account, contact, resource=None):
        SignalEmitter.__init__(self, ['rxChatMsg','txChatMsg', 'create', 'end'])

        self.account = account
        self.contact = contact
        self.target = copy.copy(contact.jid)
        if resource:
            self.target.resource = resource
        self._msgs = [] # 'Private' as the implementation may change

    def addRxMsg(self, msg):
        self._msgs.append(msg)
        self.emitsig('rxChatMsg', conv=self)

    def addTxMsg(self, msg):
        msg.tx = True
        self._msgs.append(msg)
        self.emitsig('txChatMsg', conv=self)

    def lastMsg(self):
        return self._msgs[len(self._msgs)-1]

    def end(self):
        self.emitsig('end', conv=self)


class Conversations(SignalEmitter):
    def __init__(self, account):
        SignalEmitter.__init__(self, ['create'])

        self.account = account
        self.convs = {}
        self.log = logging.getLogger("conversation")        

    def __getitem__(self, key):
        return self.convs[key.userhost()]

    def __setitem__(self, key, val):
        self.convs[key.userhost()] = val

    def addConversation(self, conv):
        self[conv.contact.jid] = conv
        self.emitsig('create', conv=conv)
        conv.addReceiver('end', self.gotEnd)

    def getConversation(self, jid):
        if jid.userhost() in self.convs:
            return self[jid]
        else:
            return None

    def getAll(self, type=None):
        if type:
            def comp(x):
                return x.type == type
            return filter(comp, self.convs.values())
        else:
            return self.conv.values()
        
    def gotEnd(self, conv):
        if conv.contact.jid.userhost() in self.convs:
            del self.convs[conv.contact.jid.userhost()]


#################### Data Forms ####################

# See http://www.jabber.org/jeps/jep-0004.html

class XOption(object):
    def __init__(label=None, value=None):
        self.label = label
        self.value = value

class XField(object):
    def __init__(self, type):
        self.type = type
    var = None
    label = None
    value = None
    desc = None
    required = False
    options = []
    retriever = None  # Useful for storing decoding function

class XDataForm(object):
    def __init__(self, type='form'):
        self.type = type
    title = None
    instructions = None
    fields = []
