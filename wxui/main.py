
from libsprained import threadedselectreactor
threadedselectreactor.install()  # NOTE: This must occur before any other Twisted imports
from twisted.internet import reactor

import logging, logging.config

from vlxmpp.xmpp import XMPPHandler
from common.model import Account
from common.config import PyConfigAccount
from common.plugins import loadPlugins
from wxui import app

logging.config.fileConfig("logging.conf")

#################### Initialise MVC ####################

# Load account, AKA "The Model"
account = PyConfigAccount()

# XMPP handler, AKA "The Controller"
xmpp = XMPPHandler(account)

# UI, AKA "The View"
view = app.WxApp(account, xmpp)

loadPlugins('plugins', account, view, xmpp)

view.run()
