
from twisted.words.xish import xpath
from common.model import XOption, XField, XDataForm
from vlxmpp.xmpp import XNS_XDATA, Stanza



#################### jabber:x:data forms ####################

def formToXml(form):
    xform = Stanza((XNS_XDATA, 'x'), attribs={'type':form.type})
    for field in form.fields:
        f = xform.addStanza('field', attribs={'type':field.type,
                                              'var':field.var})
        if field.value != None:
            f.addStanza('value', content=str(field.value))
        # FIXME: Option case?

    return xform


def xmlToForm(xml):
    xp = "//x[@xmlns='%s']" % XNS_XDATA
    items = xpath.queryForNodes(xp, xml)
    if not items:
        return
    xform = items[0]
    form = XDataForm
    for elem in xform.children:
        if elem.name == 'title':
            form.title = str(elem)
        elif elem.name == 'instructions':
            form.instructions = str(elem)

        elif elem.name == 'field':
            type = elem.attributes['type']
            field = XField(type)
            field.var = elem.attributes['var']
            if 'label' in elem.attributes:
                field.label = elem.attributes['label']

            # Subfields
            vals = xpath.queryForNodes('/field/desc', elem)
            if vals: field.desc = str(vals[0])
            vals = xpath.queryForNodes('/field/required', elem)
            if vals: field.required = True

            vals = xpath.queryForNodes('/field/value', elem)
            opts = xpath.queryForNodes('/field/option', elem)
            if type == 'boolean':
                if vals:
                    val = str(vals[0])
                    if val == '0' or val == 'false':
                        val = False
                    else:
                        val = True
                    field.value = val

            elif type == 'fixed' or \
                 type == 'hidden' or \
                 type == 'jid-single' or \
                 type == 'text-private' or \
                 type == 'text-single':
                if vals:
                   field.value = str(vals[0])
                   
            elif type == 'list-multi' or \
                 type == 'list-single' or \
                 type == 'jid-multi':
                if vals:
                   field.value = vals[0]
                for o in opts or []:
                    option = XOption()
                    if 'label' in o.attributes:
                        option.label = o.attributes['label']
                    vals = xpath.queryForNodes('/option/value', o)
                    if vals:
                        option.value = str(vals[0])
                    field.options.append(option)

            elif type == 'text-multi':
                vstr = ''
                for val in vals:
                    vstr += str(val)
                field.value = vstr

            form.fields.append(field)

    return form
