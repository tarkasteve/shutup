import unittest, sys
from common.utils import SignalEmitter

class testSignals(unittest.TestCase):
    def setUp(self):
        self.received = False

    def receiver(self):
        self.received = True

    def receiverKw(self, val=False):
        self.received = val

    def testSimple(self):
        emitter = SignalEmitter(['testsig'])
        emitter.addReceiver('testsig', self.receiver)        

        emitter.emitsig('testsig')
        self.assertEqual(self.received, True)

    def testSimpleKw(self):
        emitter = SignalEmitter(['testsig'])
        emitter.addReceiver('testsig', self.receiverKw)        

        emitter.emitsig('testsig', val=True)
        self.assertEqual(self.received, True)

    def testUnknownKw(self):
        emitter = SignalEmitter(['testsig'])
        emitter.addReceiver('testsig', self.receiverKw)        
        self.assertRaises(TypeError, emitter.emitsig, 'testsig', unknown=True)

    def testEmitUnknown(self):
        emitter = SignalEmitter(['testsig'])
        self.assertRaises(KeyError, emitter.emit, 'unknown')

    def testAddUnknown(self):
        emitter = SignalEmitter(['testsig'])
        self.assertRaises(KeyError, emitter.addReceiver, 'unknown', self.receiver)

    def testAddition(self):
        emitter = SignalEmitter(['testsig'])
        emitter.addSignal('othersig')
        emitter.addReceiver('othersig', self.receiver)        

        emitter.emitsig('othersig')
        self.assertEqual(self.received, True)


if __name__ == "__main__":
    unittest.main()
