from twisted.words.protocols import jabber
from twisted.words.protocols.jabber import client,jid
from twisted.xish import xmlstream
from twisted.internet import reactor
from twisted.python import log
from libsprained import domish
import sys

import saslclient


def rawin(e):
    print "IN :",e
def rawout(e):
    print "OUT:",e

def authd(stream):
    stream.rawDataInFn = rawin
    stream.rawDataOutFn = rawout
    print "we've authd!"

    #need to send presence so clients know we're
    #actually online
#     xmlstream.addObserver("/message",  gotMessage)
#     xmlstream.addObserver("/presence", gotPresence)
#     xmlstream.addObserver("/iq",       gotIq)
    
    presence = domish.Element(("jabber:client","presence"))
    #stream.send(presence)

    iq = client.IQ(stream,type='get')
    iq.attributes['to'] = "moth.vislab.usyd.edu.au"

    q = domish.Element(("http://jabber.org/protocol/disco#info","query"))
    iq.addChild(q)
    iq.send()

    print "gened",iq.toXml().encode('utf8')

def error(e):
    print e.toXml().encode('utf-8')
    print "we've errored!"

def gotMessage(obj):
    print "here's the message %s" % obj.toXml().encode('utf8')

def gotIq(obj):
    print "here's the iq %s" % obj.toXml().encode('utf8')

def gotPresence(obj):
    print "here's the presence %s" % obj.toXml().encode('utf8')

# log.startLogging(sys.stdout)

mech = 'DIGEST-MD5'
# mech = 'PLAIN'
# server  = 'jabber.itlab.musc.edu'
server  = 'moth.vislab.usyd.edu.au'
secret  = 'wibble'

myJid   = jid.JID('tarka@moth.vislab.usyd.edu.au/testclient')

factory = saslclient.saslClientFactory(myJid,secret,mech)
factory.addBootstrap(xmlstream.STREAM_AUTHD_EVENT,authd)
factory.addBootstrap(xmlstream.STREAM_ERROR_EVENT,error)

reactor.connectTCP(server,5222,factory)
reactor.run()
