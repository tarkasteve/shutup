# Copyright (c) 2001-2004 Twisted Matrix Laboratories.
# See LICENSE for details.


from wxPython.wx import *

#from twisted.internet import threadedselectreactor
from libsprained import threadedselectreactor
threadedselectreactor.install()
from twisted.internet import reactor

############################################################

from twisted.xish import xmlstream, xpath
from libsprained import domish
from twisted.words.protocols.jabber import client,jid

class JHandler:
    def __init__(self, jidstr, passwd):
        self.jid = jid.JID(jidstr)
        self.passwd = passwd
        self.stream = None
        self.endsent = False

    def _xrx(self,xml):
        print('RX: '+xml)

    def _xtx(self,xml):
        print("TX:"+xml)

    def connect(self, retry=0):
        print("Connection initiated")
        # Initialise connection and authorisation

        #factory = client.basicClientFactory(self.account.jid, self.account.passwd)
        ba = client.BasicAuthenticator(self.jid, self.passwd)
        factory = xmlstream.XmlStreamFactory(ba)
        #factory.continueTrying = retry  # FIXME: This should work but doesn't

        factory.addBootstrap(xmlstream.STREAM_CONNECTED_EVENT, self.connected)
        factory.addBootstrap(xmlstream.STREAM_AUTHD_EVENT, self.authorised)
        factory.addBootstrap(xmlstream.STREAM_ERROR_EVENT, self.error)
        factory.addBootstrap(xmlstream.STREAM_END_EVENT, self.gotEnd)

        reactor.connectTCP(self.jid.host, 5222, factory)

    
    def connected(self, stream):
        self.stream = stream
        # Debug IO
        stream.rawDataInFn = self._xrx
        stream.rawDataOutFn = self._xtx

    def stop(self):
        print "XMPP stopping"
        self.endstream()

    def endstream(self, stream):
        self.stream.send('</stream:stream>')
        self.endsent = True

    def gotEnd(self, stream):
        print("Received end of stream from server")
        if not self.endsent:
            self.endstream(self)

    def authorised(self, stream):
        print "Got authorised"

    def error(self,err):
        print "Got error"


############################################################

ID_CONN  = 101
ID_EXIT  = 102

class MyFrame(wxFrame):
    def __init__(self, parent, ID, title):
        wxFrame.__init__(self, parent, ID, title, wxDefaultPosition, wxSize(300, 200))
        menu = wxMenu()
        menu.Append(ID_CONN, "C&onnect", "Connect to server")
        menu.Append(ID_EXIT, "E&xit", "Terminate the program")
        menuBar = wxMenuBar()
        menuBar.Append(menu, "&File")
        self.SetMenuBar(menuBar)
        EVT_MENU(self, ID_CONN,  self.doConnect)
        EVT_MENU(self, ID_EXIT,  self.DoExit)

        self.xmpp = JHandler("tarka@moth.vislab.usyd.edu.au/TestClient", "wibble")
        reactor.interleave(wxCallAfter)

    def doConnect(self, event):
        print "Connecting"
        self.xmpp.connect()

    def DoExit(self, event):
        reactor.addSystemEventTrigger('after', 'shutdown', self.Close, true)
        reactor.stop()


class MyApp(wxApp):

    def OnInit(self):
        frame = MyFrame(NULL, -1, "Hello, world")
        frame.Show(true)
        self.SetTopWindow(frame)
        return true


def demo():
    app = MyApp(0)
    app.MainLoop()


if __name__ == '__main__':
    demo()
