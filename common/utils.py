
import sys, os, string, weakref

class SignalEmitter(object):
    """Base class for linking named signals to functions.
       Initialised with a list of signals this class will emit."""
    def __init__(self, signals):
        self._signals = {}
        for sig in signals:
            self._signals[sig] = []

    def addSignal(self, sig):
        if sig not in self._signals:
            self._signals[sig] = []

    def addReceiver(self, sig, receiver, pri=50):        
        """Add a signal receiver, throws KeyError if not available."""
        self._signals[sig].append((pri, receiver))
        self._signals[sig].sort()

    def emitsig(self, signal, **kwargs):
        """Emit a signal with keyword arguments.  The emitting object
           will be added if the function has the 'magic' keyword
           'emitter'. Throws KeyError if signal not registered."""
        for func in self._signals[signal]:
            func = func[1]
            if 'emitter' in func.func_code.co_names:
                func(emitter=self, **kwargs)
            else:
                func(**kwargs)
