#!/usr/bin/env python

from distutils.core import setup

setup(name='ShutUp',
      version='0.0.1',
      description='Vislab experimental Jabber/AG client',
      author='Steve Smith',
      author_email='ssmith@vislab.usyd.edu.au',
      url='http://www.vislab.usyd.edu.au/',
      packages=['vlxmpp',
                'common',
                'gtkui',
                'libsprained',
                'vlxmpp',
                'plugins'],
      data_files={'icons':['icons/'],
                  'images':['images/'],
                  '':['logging.conf']}
      )
