
import logging, logging.config
from twisted.internet import gtk2reactor
gtk2reactor.install()  # NOTE: This must occur before any other Twisted imports
from twisted.internet import reactor

from vlxmpp.xmpp import XMPPHandler
from common.model import Account
from gnomeui import app, config  # Do first to pull in GTK reactor
from common.plugins import loadPlugins

logging.config.fileConfig("logging.conf")

#################### Initialise MVC ####################

account = config.GConfAccount()

# XMPP handler, AKA "The Controller"
xmpp = XMPPHandler(account)

# Gnome interface, AKA "The View"
view = app.GnomeApp(account, xmpp)

loadPlugins('plugins', account, view, xmpp)

view.run()
reactor.run()
