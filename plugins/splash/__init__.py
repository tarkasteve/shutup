
import logging
from common.components import TrivialComponentPlugin, PluginException
from common.model import Contact
import splash

class MUCPlugin(TrivialComponentPlugin):
    name = "Splash plugin"
    author = "Steve Smith <ssmith@vislab.usyd.edu.au>"
    desc = "Add's a logo tab to the UI on startup"
    handle = "splash"

    def __init__(self, core):
        TrivialComponentPlugin.__init__(self)
        self.core = core

        uitype = core.view.getType()
        if uitype == 'gnome' or uitype == 'gtk':
            from splash.gtkview import SplashGtkPlugin
            self.viewplugin = SplashGtkPlugin(self)
        else:
            log.warn('Plugin does not support view-type %s, disabling' % core.view.getType())
            raise PluginException()
        core.view.addComponentPlugin(self.viewplugin)

        self.viewplugin.createView()  # Create logo tab

        log.info('Initialised Splash plugin')



def init(coreapp):
    global log
    log = logging.getLogger("splash")

    try:
        return MUCPlugin(coreapp)
    except(PluginException):
        return None
