
import logging
import pygtk
pygtk.require('2.0')

import gobject, gtk, gtk.glade, pango
import gnome, gnome.ui
from common.model import ChatMessage, Conversation
from gnomeui.config import PALETTE

class ComponentTab(gtk.HBox):
    def __init__(self, title):
        gtk.HBox.__init__(self)
        # Create tab-handle
        img = gtk.Image()
        img.set_from_stock('gtk-close', 1)

        self.close = gtk.Button()
        self.close.add(img)
        self.close.set_relief(gtk.RELIEF_NONE)

        self.label = gtk.Label(title)

        self.pack_start(self.label)
        self.pack_start(self.close)
        self.show_all()

class ChatTab(ComponentTab):
    def __init__(self, conv):
        ComponentTab.__init__(self, conv.contact.name)

class ChatView(gtk.VBox):
    def __init__(self, conv):
        gtk.VBox.__init__(self)

        self.conv = conv
        self.tview = gtk.TextView()
        self.entry = gtk.Entry()
        self.log = logging.getLogger('ChatView')

        # Configure and pack
        scr = gtk.ScrolledWindow()
        self.scrolled = scr
        scr.set_policy(gtk.POLICY_NEVER, gtk.POLICY_ALWAYS)
        scr.set_border_width(4)
        scr.set_shadow_type(gtk.SHADOW_IN)

        self.tview.set_wrap_mode(gtk.WRAP_WORD)
        self.tview.set_cursor_visible(False)
        self.tview.connect('key_press_event', self.tview_keypress)

        scr.add(self.tview)

        align = gtk.Alignment(xscale=1.0)
        align.set_border_width(4)
        align.add(self.entry)

        self.pack_start(scr)
        self.pack_end(align, expand=False)

        self.tview.set_editable(False)

        # Text properties
        buf = self.tview.get_buffer()
        self.rxtag = buf.create_tag('rxtag', foreground=PALETTE[0])
        self.txtag = buf.create_tag('txtag', foreground=PALETTE[1])

        # Wire-up
        self.entry.connect('activate', self.msgEntered)

        # Ready, subscribe to messages entering this chat
        conv.addReceiver('txChatMsg', self.txChatMsg)
        conv.addReceiver('rxChatMsg', self.rxChatMsg)

        self.show_all()

    #################### GTK Event Handlers ####################

    def tview_keypress(self, widget, event):
        # FIXME: There are probably some key events we want to act on,
        # but mostly we want to pass the key focus to the message
        # entry widget...
        if not self.entry.is_focus():
            self.entry.grab_focus()
            self.entry.emit('key_press_event', event)


    #################### Text handling ####################

    def end_visible(self):
        buf = self.tview.get_buffer()
        end = buf.get_end_iter()
        end_rect = self.tview.get_iter_location(end)
        visible = self.tview.get_visible_rect()
        return end_rect.y <= (visible.y + visible.height)

    def scroll_to_end(self):
        adj = self.scrolled.get_vadjustment()
        adj.set_value(adj.upper)

        buf = self.tview.get_buffer()
        end = buf.get_end_iter()
        self.tview.scroll_to_iter(end, 0, True, 0, 1)


    def taggedprint(self, tag, txt):
        buf = self.tview.get_buffer()

        start = buf.create_mark('start', buf.get_end_iter(), True)
        end = buf.get_end_iter()
        buf.insert(end, txt)
        buf.apply_tag(tag, buf.get_iter_at_mark(start), end)

        buf.delete_mark(start)

    def println(self, txt, scroll=True):
        buf = self.tview.get_buffer()
        buf.insert(buf.get_end_iter(), txt+'\n')
        if scroll:
            self.scroll_to_end()

    def msgEntered(self, entry):
        txt = entry.get_text()
        self.log.info("Message entered on GUI: "+txt)
        self.conv.addTxMsg(ChatMessage(txt, tx=True))
        entry.set_text('')

    def rxChatMsg(self, conv):
        self.log.info("GUI got RX conversation message")
        msg = conv.lastMsg()
        assert(not msg.tx)

        if msg.hidden:
            return
        self.taggedprint(self.rxtag, conv.contact.name+": ")
        self.println(msg.txt, scroll=self.end_visible())

    def txChatMsg(self, conv):
        self.log.info("GUI got TX conversation message")
        msg = conv.lastMsg()
        assert(msg.tx)

        self.taggedprint(self.txtag, conv.account.jid.user+": ")
        self.println(msg.txt, scroll=self.end_visible())

