
import os, os.path
import logging
from ConfigParser import ConfigParser, NoOptionError, NoSectionError, DuplicateSectionError
from common.model import Account

CONFDIR = os.path.expanduser('~/.shutup/')

class PyConfig(object):
    def __init__(self, filename):
        if not os.path.isdir(CONFDIR):
            os.mkdir(CONFDIR, 0700)

        self.path = CONFDIR + filename
        self.log = logging.getLogger("config")
        self.log.info("Loading config file %s" % self.path)

        self.config = ConfigParser()
        self.config.read(self.path)

    def save(self):        
        fd = open(self.path, 'w+')
        self.config.write(fd)
        fd.close()

class PyConfigAccount(Account, PyConfig):
    section = 'Account'
    def __init__(self):
        PyConfig.__init__(self, 'accountrc')

        config = self.config
        try:
            self.configured = self.config.getboolean(self.section, 'configured')
            jidstr = config.get(self.section,'jid')
            passwd = config.get(self.section,'passwd')
            ssl = config.getboolean(self.section,'ssl')
        except(NoOptionError,NoSectionError):            
            self.configured = False

        if self.configured:
            Account.__init__(self, jidstr, passwd, ssl=ssl)
        else:
            self.log.warn("No configuration")
            Account.__init__(self)

    def flush(self):
        try:
            self.config.add_section(self.section)
        except(DuplicateSectionError):            
            pass
        self.config.set(self.section, 'configured', True)
        self.config.set(self.section, 'jid',self.jid.full())
        self.config.set(self.section, 'passwd', self.passwd)
        self.config.set(self.section, 'ssl', self.ssl)
        self.save()

