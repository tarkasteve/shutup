
import logging, datetime, os, string
from common.components import TrivialComponentPlugin, PluginException
from common.model import Contact, Conversation, Conversations
from common.config import CONFDIR


class ChatLog(object):
    def __init__(self, core, conv):
        self.core = core
        self.conv = conv
        self.clog = None

        dirname = CONFDIR + 'logs/'
        datestr = datetime.datetime.now().strftime('%Y%m%d')
        filename = string.replace(conv.target.full(), '/', '_')
        filename = '%s-%s.log'%(filename, datestr)

        # FIXME: error handling
        if not os.path.isdir(dirname):
            os.mkdir(dirname, 0700)
        self.clog = open(dirname+filename, 'a')
        self.writeline('Conversation with %s opened'%conv.target.full())

        conv.addReceiver('rxChatMsg', self.rx)
        conv.addReceiver('txChatMsg', self.rx)

    def rx(self, conv):
        log.info("Got conversation message")
        msg = self.conv.lastMsg()
        if msg.hidden:
            return
        if msg.sender:
            sender = msg.sender.full()
        else:
            sender = self.core.account.jid.user
        self.writeline('%s: %s'%(sender, msg.txt))

    def writeline(self, str):
        self.clog.write(datetime.datetime.now().strftime('[%H:%M:%S] '))
        self.clog.write(str)
        self.clog.write('\n')

    def end(self):
        # Cleanup
        if self.clog:
            self.clog.close()


class ChatLogPlugin(TrivialComponentPlugin):
    """Main handler for logging chats"""

    name = "Logging Plugin"
    author = "Steve Smith <ssmith@vislab.usyd.edu.au>"
    desc = "Adds logging of all conversations."
    handle = "chatlog"

    def __init__(self, core):
        TrivialComponentPlugin.__init__(self)
        self.core = core
        core.account.conversations.addReceiver('create', self.newConv)
        self.logs = {}

        log.info('Initialised ChatLog plugin')

    def newConv(self, conv):
        target = conv.target.full()
        log.info("ChatLog got new conversation with %s"%target)
        self.logs[conv] = ChatLog(self.core, conv)
        conv.addReceiver('end', self.endConv)

    def endConv(self, conv):
        target = conv.target.full()
        log.info("ChatLog got end of conversation with %s"%target)
        if conv in self.logs:
            self.logs[conv].end()
            del self.logs[conv]


def init(coreapp):
    global log
    log = logging.getLogger("chatlog")

    try:
        return ChatLogPlugin(coreapp)
    except(PluginException):
        return None
