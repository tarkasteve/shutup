#!/usr/bin/python
# -*- coding: koi8-r -*-
from xmpp import *

def presenceHandler(conn,presence_node):
    """ Handler for playing a sound when particular contact became online """
    targetJID='node@domain.org'
    if presence_node.getFrom().bareMatch(targetJID):
        # play a sound
        pass
def iqHandler(conn,iq_node):
    """ Handler for processing some "get" query from custom namespace"""
    reply=iq_node.buildReply('result')
    # ... put some content into reply node
    conn.send(reply)
    raise NodeProcessed  # This stanza is fully processed
def messageHandler(conn,mess_node): pass

# Born a client
cl=Client('moth.vislab.usyd.edu.au', debug=None)
# ...connect it to SSL port directly
if not cl.connect():
    raise IOError('Can not connect to server.')

# ...authorize client
print "Auth"
if not cl.auth('tarka','wibble','pyclient'):
    raise IOError('Can not auth with server.')
# ...register some handlers (if you will register them before auth they will be thrown away)

print "register"
cl.RegisterHandler('presence',presenceHandler)
cl.RegisterHandler('iq',iqHandler)
cl.RegisterHandler('message',messageHandler)

print "send presence"
# ...become available
cl.sendInitPresence()

# ...work some time
print "Processing...",
cl.Process(1)
print "done"
# ...if connection is brocken - restore it
if not cl.isConnected():
    print "Reconnecting"
    cl.reconnectAndReauth()

# ...send an ASCII message
cl.send(Message('ssmith@jabber.vislab.usyd.edu.au','Test message'))

# ...send a national message
cl.send(Message('ssmith@jabber.vislab.usyd.edu.au',
                unicode('�������� �����','koi8-r')))

# ...send another national message
simplexml.ENCODING='koi8-r'
cl.send(Message('ssmith@jabber.vislab.usyd.edu.au','�������� ����� 2'))
# ...work some more time - collect replies
cl.Process(1)
# ...and then disconnect.
cl.disconnect()

"""
If you have used jabberpy before you will find xmpppy very similar.
See the docs for more info about library features.
"""
