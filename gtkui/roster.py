
import logging, pytz, datetime
import pygtk
pygtk.require('2.0')

import gobject, gtk, gtk.glade, pango
from cell_renderer_image import CellRendererImage
from common.model import JID, Conversation, Contact
from common.components import View, TrivialComponentPlugin, TrivialComponent
from gtkui.config import PRESTYPES, PRESCOLOR, getIcons
from gtkui.components import TrivialGTKPlugin, TrivialGTKComponent


class GtkRoster(gtk.TreeView):

    def __init__(self, core):
        gtk.TreeView.__init__(self)

        self.core = core
        self.xmpp = core.xmpp
        self.account = core.account
        self.defaultAction = None

        self.log = logging.getLogger("gui")

        self.account.roster.addReceiver('gotContact', self.gotContact)
        self.account.roster.addReceiver('updateContact', self.gotContactUpdate)
        self.account.roster.addReceiver('gotPresence', self.gotContactPresence)

        # Setup model and view
        self.reset()

        self.set_headers_visible(False)
        
        cell = CellRendererImage()
        col = gtk.TreeViewColumn()
        col.pack_start(cell, expand = False)
        col.add_attribute(cell, 'image', 1)

        cell = gtk.CellRendererText()
        cell.set_property('xpad', 5)
        col.pack_start(cell, expand = False)
        col.add_attribute(cell, 'text', 0)
        self.append_column(col)

        self.connect('button-press-event', self.clicked)


    def reset(self):
        ts = gtk.TreeStore(gobject.TYPE_STRING,   # Name
                           gobject.TYPE_OBJECT,   # Status image
                           gobject.TYPE_STRING,   # Font Weight
                           gobject.TYPE_STRING,   # Type (group, user, etc)
                           gobject.TYPE_PYOBJECT) # Contact object
        #ts.set_sort_func(1, self.compareIters)
        ts.set_sort_column_id(0, gtk.SORT_ASCENDING)
        self.set_model(ts)


    #################### Model Event Handlers ####################
        
    def getGroup(self, gname):
        ts = self.get_model()
        iter = ts.get_iter_first()
        while iter:
            val = ts.get_value(iter, 0)
            if val == gname:
                return iter
            iter = ts.iter_next(iter)

        # Failed, create group tree
        iter = ts.append(None, (gname, None, pango.WEIGHT_BOLD, 'group', None))
        return iter

    def gotContact(self, contact):
        self.log.debug("GUI got new contact")
        giter = self.getGroup(contact.group or "UnGrouped")
        ts = self.get_model()
        row = ts.append(giter, (contact.name,
                                getIcons()['offline'],
                                pango.WEIGHT_NORMAL,
                                'contact',
                                contact))
        # Store ref to created row with model
        ref = gtk.TreeRowReference(ts, ts.get_path(row))
        contact.udat = ref
        # FIXME: Force expand of this group; is the right thing?
        self.expand_row(ts.get_path(giter), True)

    def gotContactUpdate(self, contact):
        self.log.info("GUI got contact update")
        # FIXME

    def gotContactPresence(self, user):
        self.log.info("GUI got presence for user "+user.name+'='+user.presence)
        if user.presence not in getIcons():
            return
        ref = user.udat
        if not ref:
            self.log.error("No GUI item for user "+user.name)
            return
        
        ts = self.get_model()
        row = ts.get_iter(ref.get_path())
        ts.set_value(row, 1, getIcons()[user.presence])


    #################### GTK Signal Handlers ####################

    def click_to_contact(self, roster, event):
        path, col, x, y = roster.get_path_at_pos(int(event.x), 
                                                 int(event.y))
        if not path:
            return None
        
        # Lookup user data attached to this row
        model = roster.get_model()
        iter = model.get_iter(path)
        contact = model.get_value(iter, 4)
        return contact

    def clicked(self, roster, event):
        if event.type == gtk.gdk.BUTTON_PRESS and event.button == 3: # Right click
            contact = self.click_to_contact(roster, event)
            if not contact:
                return
            self.show_contact_menu(event, contact)

        elif event.type == gtk.gdk._2BUTTON_PRESS:
            contact = self.click_to_contact(roster, event)
            if not contact:
                return
            self.defaultAction(contact)

    def show_contact_menu(self, event, contact):
        menu = gtk.Menu()

        # Plugins
        for p in self.core.view.plugins:
            for gen in p.rosterctxitems:
                menu.append(gen(contact))

        menu.popup(None, None, None, event.button, event.time)
        menu.show_all()
