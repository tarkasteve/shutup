
import logging
from common.components import TrivialComponentPlugin, PluginException
from common.model import Contact
#import chat
from common.model import Conversation
from xmpp import ChatXMPP

class ChatPlugin(TrivialComponentPlugin):
    """Main handler for Chat GUI events not attached to particular chat
       session (e.g) menu items"""

    name = "Core Chat"
    author = "Steve Smith <ssmith@vislab.usyd.edu.au>"
    desc = "Add's basic chat functionality"
    handle = "chat"

    def __init__(self, core):
        TrivialComponentPlugin.__init__(self)
        self.core = core

        self.chatxmpp = ChatXMPP(core, self)

        uitype = core.view.getType()
        if uitype == 'gnome' or uitype == 'gtk':
            from gtkview import ChatGtkPlugin
            self.viewplugin = ChatGtkPlugin(self)
        else:
            log.warn('Chat plugin does not support view-type %s, disabling' % core.view.getType())
            raise PluginException()
        core.view.addComponentPlugin(self.viewplugin)

        log.info('Initialised Chat plugin')

#     def createComponent(self, jid):
#         # Just check it's not there already
#         account = self.core.account
#         if account.conversations.getConversation(jid):
#             return 

#         conv = ChatConversation(account, Contact(jid))
#         account.conversations.addConversation(conv)

#         xmppcomp = self.chatxmpp.createChat(conv)

#         chat = self.viewplugin.createView(conv, xmppcomp)


def init(coreapp):
    global log
    log = logging.getLogger("chat")

    try:
        return ChatPlugin(coreapp)
    except(PluginException):
        return None
