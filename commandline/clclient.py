#
# XMPP 'Adventure' client.
#
import sys,logging.config
from twisted.internet import stdio, reactor
from commandline import CommandLine
from twisted.xish import xmlstream
from vlxmpp import xmpp
from common.model import Account

########## Start ##########

logging.config.fileConfig("logging.conf")

#server  = 'moth.vislab.usyd.edu.au'
secret  = 'wibble'

if len(sys.argv) == 2:
    jid = sys.argv[1]
else:    
    jid = 'tarka@moth.vislab.usyd.edu.au/clclient'

account = Account(jidstr=jid,passwd=secret)
xmpp = xmpp.XMPPHandler(account)
cmdl = CommandLine(account, xmpp)

# Wire-up
stdio.StandardIO(cmdl)

# Go
xmpp.connect()
reactor.run()
