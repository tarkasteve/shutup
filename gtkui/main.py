
import logging, logging.config
from twisted.internet import gtk2reactor
gtk2reactor.install()  # NOTE: This must occur before any other Twisted imports
from twisted.internet import reactor

from vlxmpp.xmpp import XMPPHandler
from common.model import Account
from gtkui.app import GTKView
from common.config import PyConfigAccount
from common.components import Application

class GTKApplication(Application):
    def run(self):
        logging.config.fileConfig("logging.conf")

        ## Initialise MVC
        self.account = PyConfigAccount()
        
        # XMPP handler, AKA "The Controller"
        self.xmpp = XMPPHandler(self.account)

        # GTK interface, AKA "The View"
        self.view = GTKView(self)
        
        self.loadPlugins()
        
        self.view.run()
        reactor.run()

app = GTKApplication()
app.run()
