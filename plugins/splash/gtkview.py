
import string, socket
import gobject, gtk, gtk.glade, pango, gconf
import gtkui

from gtkui.components import TrivialGTKPlugin, TrivialGTKComponent
from common.const import LOGO_IMAGE
import splash


class SplashGtkComponent(gtk.Image, TrivialGTKComponent):
    def __init__(self):
        gtk.Image.__init__(self)
        TrivialGTKComponent.__init__(self, 'Splash')

        self.set_from_file(LOGO_IMAGE)
        self.show_all()


class SplashGtkPlugin(TrivialGTKPlugin):
    """Main handler for MUC GUI events not attached to particular chat
       session (e.g) menu items"""
    def __init__(self, plugin):
        TrivialGTKPlugin.__init__(self)
        self.plugin = plugin

    def createView(self):
        view = SplashGtkComponent()
        self.emitsig('create', component=view)
        return view
