#
# XMPP 'Adventure' client.
#
import sys
from string import split, rstrip
from twisted.internet import stdio, reactor
from twisted.xish import xpath
from twisted.protocols.basic import LineReceiver
from libsprained import domish
from vlxmpp.xmpp import IQ
from common.model import JID

sdp="""v=0
s=Access Grid Lobby
i=A buzzing hub of activity. Doors to several conference rooms open into this space.
m=video 17018 RTP/AVP 31
c=IN IP4 233.2.178.9/127
m=audio 17008 RTP/AVP 112
c=IN IP4 233.2.178.9/127
"""


def sdpreq():
    iq = IQ('get', {'to':'vislab@conference.moth.vislab.usyd.edu.au'})
    iq.addStanza('query', xns="http://vislab.usyd.edu.au/protocol/muc#sdp")
    return iq

def storexml():
    iq = IQ('set', {'to':'vislab@conference.moth.vislab.usyd.edu.au'})
    q = iq.addStanza('query', xns="jabber:iq:private")
    cfg  = q.addStanza('cfg', xns='http://vislab.usyd.edu.au/protocol/xsdp')

    c = cfg.addStanza('component', attribs={'media':'video', 'name':'AG Video'})
    alt = c.addStanza('alt', attribs={'name':'AG-video-mcast',
                                      'rtp-port':'17018',
                                      'ttl':'127',
                                      'addr':'233.2.178.9',
                                      'format':'rtp-avp-31'})

    c = cfg.addStanza('component', attribs={'media':'audio', 'name':'AG Audio'})
    alt = c.addStanza('alt', attribs={'name':'AG-audio-mcast',
                                      'rtp-port':'17008',
                                      'ttl':'127',
                                      'addr':'233.2.178.9',
                                      'format':'rtp-avp-112'})
    return iq


def storeencxml():
    iq = IQ('set', {'to':'secret@conference.moth.vislab.usyd.edu.au'})
    q = iq.addStanza('query', xns="jabber:iq:private")
    cfg  = q.addStanza('cfg', xns='http://vislab.usyd.edu.au/protocol/xsdp')

    c = cfg.addStanza('component', attribs={'media':'video', 'name':'AG Video'})
    alt = c.addStanza('alt', attribs={'name':'AG-video-mcast',
                                      'rtp-port':'49624',
                                      'ttl':'127',
                                      'addr':'224.2.129.95',
                                      'format':'rtp-avp-31'})
    #enc = alt.addStanza('encryption', attribs={'type':'broker'})
    enc = alt.addStanza('encryption', attribs={'type':'default'},
                        content='notreallysecure')

    c = cfg.addStanza('component', attribs={'media':'audio', 'name':'AG Audio'})
    alt = c.addStanza('alt', attribs={'name':'AG-audio-mcast',
                                      'rtp-port':'65396',
                                      'ttl':'127',
                                      'addr':'224.2.139.125',
                                      'format':'rtp-avp-112'})
    #enc = alt.addStanza('encryption', attribs={'type':'broker'})
    enc = alt.addStanza('encryption', attribs={'type':'default'},
                        content='notreallysecure')

    return iq

def getxml():
    iq = IQ('get', {'to':'vislab@conference.moth.vislab.usyd.edu.au'})
    q = iq.addStanza('query', xns="jabber:iq:private")
    q.addStanza('cfg', xns='http://vislab.usyd.edu.au/protocol/xsdp')
    return iq


class CommandLine(LineReceiver):
    from os import linesep as delimiter

    # Some flags
    msgmode = False
    to = None
    msgtext = None

    def __init__(self, account, xmpp):
        self.xmpp = xmpp

        # Subscribe to updates to model
        self.account = account
        #account.addReceiver('connectedUpdate', self.connectionMade)
        account.addReceiver('gotAuthorised', self.gotAuthorised)
        #account.roster.addReceiver('gotRoster', self.gotRoster)
        #account.roster.addReceiver('gotPresence', self.gotContactPresence)
        #account.conversations.addReceiver('newConversation', self.newConversation)
        

    def gotAuthorised(self, authed):
        if authed:
            self.println("Connected and authorised")
        self.account.presence = 'available'

    def println(self, msg):
        print
        print msg
        self.prompt()

    def lineReceived(self, line):
        if self.msgmode:
            self.msgline(line)
        else:
            cmds = split(line)
            if len(cmds):
                cmd = cmds[0]
                if cmd == 'disco':
                    self.xmpp.queryDiscoInfo(cmds[1])
                elif cmd == 'msg':
                    self.msgstart(cmds[1])
                elif cmd == 'createnode':
                    self.xmpp.createNode(cmds[1])
                elif cmd == 'delnode':
                    self.xmpp.delNode(cmds[1])
                elif cmd == 'setnode':
                    self.xmpp.setNode(cmds[1])
                elif cmd == 'setsdp':
                    self.xmpp.setItem('home/moth.vislab.usyd.edu.au/tarka/muc/vislab',
                                      'multicast', sdp)
                elif cmd == 'getsdp':
                    self.xmpp.getItem('pubsub.moth.vislab.usyd.edu.au',
                                      'home/moth.vislab.usyd.edu.au/tarka/muc/vislab',
                                      'multicast')
                elif cmd == 'getnode':
                    self.xmpp.getNode(cmds[1])
                elif cmd == 'subnode':
                    rec = len(cmds) == 2
                    self.xmpp.subNode(rec)
                elif cmd == 'getsubs':
                    self.xmpp.getSubs()
                elif cmd == 'unsub':
                    rec = len(cmds) == 2
                    self.xmpp.unsub(rec)
                elif cmd == 'sdpreq':
                    self.xmpp.send(sdpreq())
                elif cmd == 'xs':
                    self.xmpp.send(storexml())
                elif cmd == 'xsg':
                    self.xmpp.send(getxml())
                elif cmd == 'pres':
                    if len(cmds) == 2:
                        self.account.presence = cmds[1]
                    else:
                        self.account.presence = 'available'

                
        self.prompt()

    def msgstart(self, to):
        self.to = to
        self.msgmode = True
        self.msgtext = ""

    def msgline(self, line):
        if line == '.':
            self.msgsend()
        else:
            self.msgtext += line

    def msgsend(self):
        self.sendMessage(self.to, self.msgtext)
        self.msgmode = False

    def prompt(self):
        if self.msgmode:
            self.transport.write('" ')
        else:
            self.transport.write('> ')

    def xmlin(self,str):
        print "RX:",str
        pass
        
    def xmlout(self,str):
        print "TX:",str
        pass
