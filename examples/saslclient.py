# need some tests, etc
# Creator : Christopher Zorn

from twisted.words.protocols import jabber
from twisted.words.protocols.jabber import client,jid
from twisted.xish import xpath, utility, xmlstream
from libsprained import domish

import md5, base64, time, os, random, binascii

TLS_XMLNS = 'urn:ietf:params:xml:ns:xmpp-tls'
SASL_XMLNS = 'urn:ietf:params:xml:ns:xmpp-sasl'
BIND_XMLNS = 'urn:ietf:params:xml:ns:xmpp-bind'
SESSION_XMLNS = 'urn:ietf:params:xml:ns:xmpp-session'

def saslClientFactory(jid, secret, mech = "DIGEST-MD5"):
    a = SASLAuthenticator(jid, secret, mech)
    return xmlstream.XmlStreamFactory(a)


class SASLAuthenticator(client.BasicAuthenticator):
    """ Authenticates an XmlStream against a Jabber server as a Client

    This only implements SASL authentication. Additionally, this
    authenticator provides the ability to perform inline registration, per
    U{JEP-0077<http://www.jabber.org/jeps/jep-0077.html>}.

    Under normal circumstances, the SASLAuthenticator generates the
    L{xmlstream.STREAM_AUTHD_EVENT} once the stream has authenticated. However,
    it can also generate other events, such as:
      - L{INVALID_USER_EVENT} : Authentication failed, due to invalid username
      - L{AUTH_FAILED_EVENT} : Authentication failed, due to invalid password
      - L{REGISTER_FAILED_EVENT} : Registration failed

    If authentication fails for any reason, you can attempt to register by
    calling the L{registerAccount} method. If the registration succeeds, a
    L{xmlstream.STREAM_AUTHD_EVENT} will be fired. Otherwise, one of the above
    errors will be generated (again).
    
    """
    namespace = "jabber:client"
    version   = 0.0
    success   = 0

    def __init__(self,jid,password,mech="DIGEST-MD5"):
        self.mechanism = mech
        client.BasicAuthenticator.__init__(self,jid,password)
        
    def connectionMade(self):
        # Generate stream header
        sh = "<stream:stream xmlns='%s' xmlns:stream='http://etherx.jabber.org/streams' version='1.0' to='%s'>" % \
             (self.namespace, self.streamHost.encode('utf-8'))
        
        self.xmlstream.send(sh)


    def streamStarted(self, rootelem):
        if rootelem.hasAttribute('version'):
            self.version = float(rootelem['version'])
        if self.version == 1.0:
            # LOOK for features
            self.xmlstream.addOnetimeObserver("/features",self._featureParse)
        else:
            iq = IQ(self.xmlstream, "get")
            iq.addElement(("jabber:iq:auth", "query"))
            iq.query.addElement("username", content = self.jid.user)
            iq.addCallback(self._authQueryResultEvent)
            iq.send()
            
    def _reset(self):
        self.xmlstream.stream = domish.elementStream()
        self.xmlstream.stream.DocumentStartEvent = self.xmlstream.onDocumentStart
        self.xmlstream.stream.ElementEvent = self.xmlstream.onElement
        self.xmlstream.stream.DocumentEndEvent = self.xmlstream.onDocumentEnd
        # Generate stream header
        if self.version == 1.0:
            sh = "<stream:stream xmlns='%s' xmlns:stream='http://etherx.jabber.org/streams' version='1.0' to='%s'>" % \
                 (self.namespace, self.streamHost.encode('utf-8'))
        else:
            sh = "<stream:stream xmlns='%s' xmlns:stream='http://etherx.jabber.org/streams' to='%s'>" % \
                 (self.namespace, self.streamHost.encode('utf-8'))
        self.xmlstream.send(sh)

        
    def _featureParse(self, f):
        self.bind    = 0
        self.session = 0
        # TODO - check for tls
        if self.success == 1:
            for f in f.elements():
                if f.name == "bind":
                    self.bind = 1
                if f.name == "session":
                    self.session = 1
                    
            if self.bind == 1:
                iq = client.IQ(self.xmlstream, "set")
                iq.addElement((BIND_XMLNS, "bind"))
                
                iq.bind.addElement("resource", content = self.jid.resource)
                iq.addCallback(self._bindResultEvent)
                iq.send()
                
            #if self.session == 1:
                #print "We need to start a session"
                
        else:
            m = f.mechanisms

            if m.uri == SASL_XMLNS:
                for mech in m.elements():
                    ms = str(mech)
                    if ms == self.mechanism:
                        break
                
                auth = domish.Element((SASL_XMLNS,"auth"),SASL_XMLNS,{'mechanism' : ms})
                # why?
                auth['xmlns'] = SASL_XMLNS
                #auth['mechanism'] = ms
                if ms == 'DIGEST-MD5':
                    self.xmlstream.addOnetimeObserver("/challenge",self._saslStep1)
                if ms == 'PLAIN':
                    # TODO add authzid
                    auth_str = ""
                    auth_str = auth_str + "\000"
                    auth_str = auth_str + self.jid.user.encode('utf-8')
                    auth_str = auth_str + "\000"
                    auth_str = auth_str + self.password.encode('utf-8')
                    auth.addContent(binascii.b2a_base64(auth_str))
                    self.xmlstream.addOnetimeObserver("/success",self._saslSuccess)
                
                self.xmlstream.addObserver("/failure",self._saslError)
                self.xmlstream.send(auth)
            else:
                self.xmlstream.dispatch(f, self.AUTH_FAILED_EVENT)

    # session stuff
    def _sessionResultEvent(self, iq):
        if iq["type"] == "result":
            self.xmlstream.dispatch(self.xmlstream,
                                    xmlstream.STREAM_AUTHD_EVENT)            
        else:
            self.xmlstream.dispatch(iq, self.AUTH_FAILED_EVENT)
            
    # BIND stuff
    def _bindResultEvent(self, iq):
        if iq["type"] == "result":
            self.bind = 1
            if self.session == 1:
                iq = client.IQ(self.xmlstream, "set")
                iq.addElement((SESSION_XMLNS, "session"),content = self.jid.full())
                
                iq.addCallback(self._sessionResultEvent)
                iq.send()
                return
                
        else:
            self.bind = 0
            print "ERROR" + iq.toXml().encode('utf-8')

        if self.bind == 1 and self.session == 1:                        
            self.xmlstream.dispatch(self.xmlstream,
                                    xmlstream.STREAM_AUTHD_EVENT)            
        else:
            self.xmlstream.dispatch(iq, self.AUTH_FAILED_EVENT)
            
    # SASL stuff

    def _saslError(self, error):
        print "AUTH ERROR " + error.toXml().encode('utf8')
        self.xmlstream.dispatch(error, self.AUTH_FAILED_EVENT)
        
    def _saslStep1(self, challenge):
        c = str(challenge)

        dc = base64.decodestring(c)
        ra = self._parse(dc)
        try:
            self.realm = ra['realm']
        except(KeyError):
            self.realm = None
        self.nonce = ra['nonce']
        self.nc=0
        self.charset = ra['charset']
        self.algorithm = ra['algorithm']
        response = domish.Element((SASL_XMLNS,"response"))
        # why?
        response['xmlns'] = SASL_XMLNS
        r = self._response(self.charset,self.realm,self.nonce)
        
        response.addContent(r)
        self.xmlstream.removeObserver("/challenge",self._saslStep1)
        self.xmlstream.addOnetimeObserver("/challenge",self._saslStep2)
        self.xmlstream.send(response)


    def _saslStep2(self, challenge):
        cs = base64.decodestring(str(challenge))
        ca = self._parse(cs)
        
        if self.rauth == ca['rspauth']:
            response = domish.Element((SASL_XMLNS,"response"))
            # why?
            response['xmlns'] = SASL_XMLNS
                        
            self.xmlstream.removeObserver("/challenge",self._saslStep2)
            self.xmlstream.addOnetimeObserver("/success",self._saslSuccess)
            self.xmlstream.send(response)
        else:
            self.xmlstream.dispatch(challenge, self.AUTH_FAILED_EVENT)

    def _saslSuccess(self, s):
        self.success = 1
        self._reset()

    # SASL stuff - maybe put this in its own class?
    def _response(self, charset, realm, nonce):
        rs = ''
        try:
            username=self.jid.user.encode(charset)
        except UnicodeError:
            # TODO - add error checking 
            raise
        rs = rs + 'username="%s"' % username
        rs = rs + ',realm="%s"' % realm
        cnonce = self._gen_nonce()
        rs = rs + ',cnonce="%s"' % cnonce
        rs = rs + ',nonce="%s"' % nonce

        self.nc+=1
        nc="%08x" % self.nc
        rs = rs + ',nc=%s' % nc
        rs = rs + ',qop=auth'

        rs = rs + ',digest-uri="xmpp/'+self.jid.host+'"'
        

        uh = "%s:%s:%s" % (username,realm,self.password.encode("utf-8"))
        huh = md5.new(uh).digest()
        # TODO - add authzid
        a1 = "%s:%s:%s" % (huh,nonce,cnonce)
        a2="AUTHENTICATE:xmpp/"+self.jid.host

        a3=":xmpp/"+self.jid.host

        resp1 = "%s:%s:%s:%s:%s:%s" % (binascii.b2a_hex(md5.new(a1).digest()),
                                       nonce,
                                       nc,
                                       cnonce,
                                       "auth",
                                       binascii.b2a_hex(md5.new(a2).digest()))
        
        resp2 = "%s:%s:%s:%s:%s:%s" % (binascii.b2a_hex(md5.new(a1).digest()),
                                       nonce,nc,cnonce,
                                       "auth",binascii.b2a_hex(md5.new(a3).digest()))
        
        kda1 = md5.new(resp1).digest()
        kda2 = md5.new(resp2).digest()
        
        response = binascii.b2a_hex(kda1) 
        
        rs = rs + ',response="%s"' % response
        rs = rs + ',charset=%s' % charset
        
        self.rauth = binascii.b2a_hex(kda2)
        
        return  binascii.b2a_base64(rs)
        
    def _parse(self, rcs):
        r = rcs.split(',')
        h = {}
        for i in r:
            (k,v) = i.split('=')
            v = v.replace("'","")
            v = v.replace('"','')
            if h.has_key(k):
                # return an error
                return 0
            h[k] = v
        return h

    def _gen_nonce(self):
        return md5.new("%s:%s:%s" % (str(random.random()) , str(time.gmtime()),str(os.getpid()))).hexdigest()
