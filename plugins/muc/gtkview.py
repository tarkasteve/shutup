
import string, socket
from ConfigParser import ConfigParser, NoOptionError, DuplicateSectionError, NoSectionError
import gobject, gtk, gtk.glade, pango, gconf
import gtkui
from gtkui.components import TrivialGTKPlugin, TrivialGTKComponent
from gtkui.chat import ChatView
from gtkui.xform import XFormDialog
from common.model import Contact
from common.utils import SignalEmitter
from common.config import PyConfig
import muc
from muc.model import MucJID, MUCConversation, MUCMessage, MUCService


def getWidgets():
    return gtk.glade.XML('plugins/muc/mucui.glade')


class MUCRecent(PyConfig):
    recdelim = ','  # Between entries
    jiddelim = ':'  # Within entry
    section = 'MUC'
    def __init__(self):
        PyConfig.__init__(self, 'mucrc')
        self.jids = []

        try:
            lst = self.config.get(self.section,'recent')
            lst = lst.split(self.recdelim)
        except(NoOptionError,NoSectionError):
            lst = []
        for i in lst:
            pw, jstr = i.split(self.jiddelim)
            jid = MucJID(str=jstr)
            if pw != '':
                jid.passwd = pw
            self.jids.append(jid)

    def flush(self):
        lst = []
        for j in self.jids:
            if j.passwd:
                pw = j.passwd
            else:
                pw = ''
            jstr = pw + self.jiddelim + j.full()
            if jstr not in lst:
                lst.append(jstr)

        try:
            self.config.add_section(self.section)
        except(DuplicateSectionError):            
            pass
        self.config.set(self.section, 'recent', string.join(lst, self.recdelim))
        self.save()

    def toListStore(self):
        ls = gtk.ListStore(gobject.TYPE_STRING,   # JID str
                           gobject.TYPE_PYOBJECT) # Jid
        for j in self.jids:
            ls.append([j.full()[0:30], j])
        return ls


class MUCMemberView(gtk.TreeView):
    def __init__(self, roster, xmpp):
        # GTK view setup
        gtk.TreeView.__init__(self)
        ls = gtk.TreeStore(gobject.TYPE_STRING,   # Name
                           gobject.TYPE_PYOBJECT, # Associated object
                           gobject.TYPE_STRING)   # Type
        self.set_model(ls)

        col = gtk.TreeViewColumn("Members", gtk.CellRendererText(),
                                 text=0)
        self.append_column(col)
        self.connect('button-press-event', self.clicked)

        # Hook into model
        self.roster = roster

        roster.addReceiver('add', self.add)
        roster.addReceiver('update', self.update)
        roster.addReceiver('remove', self.remove)

    # Double-click handler
    def clicked(self, widget, event):
        if event.type == gtk.gdk._2BUTTON_PRESS:  # Double-click
            path, col, x, y = widget.get_path_at_pos(int(event.x), 
                                                     int(event.y))
            if not path:
                return

            # Lookup user data attached to this row
            model = self.get_model()
            iter = model.get_iter(path)
            type = model.get_value(iter, 2)
            if type == 'service':
                service = model.get_value(iter, 1)
                if not service:
                    return
                #service.activate()
                
                # FIXME: Hack until I come up with a better method of
                # handling service activation.
                self.lproc.activate(service)


    def add(self, member):
        ls = self.get_model()
        disp = "%s (%s/%s)" % (member.jid.resource, member.affiliation, member.role)
        row = ls.append(None, [disp, member, 'member'])
        lref = gtk.TreeRowReference(ls, ls.get_path(row))

        # Store ref to trackback
        member.udat['listref'] = lref

        for serv in member.services:
            srow = ls.append(row, [serv.name, member, 'service'])
        member.services.udat['parentref'] = lref
        member.services.addReceiver('add', self.serviceAdd)
        #member.services.addReceiver('remove', self.serviceRemove)

    def update(self, member):
        # Trackback (see above)
        ls = self.get_model()
        ref = member.udat['listref']
        row = ls.get_iter(ref.get_path())
        ls.set(row, 0, "%s (%s/%s)" % (member.jid.resource, member.affiliation, member.role), 1, member)
    def remove(self, member):
        # Trackback (see above)
        ls = self.get_model()
        ref = member.udat['listref']
        row = ls.get_iter(ref.get_path())
        ls.remove(row)

    def serviceAdd(self, emitter, service):
        ls = self.get_model()
        ref = emitter.udat['parentref']
        parent = ls.get_iter(ref.get_path())
        srow = ls.append(parent, [service.name, service, 'service'])


class MUCServiceView(gtk.TreeView, SignalEmitter):
    def __init__(self, services, xmppcomp):
        # GTK view setup
        gtk.TreeView.__init__(self)
        SignalEmitter.__init__(self, ['service_activated'])

        ls = gtk.ListStore(gobject.TYPE_STRING,
                           gobject.TYPE_PYOBJECT)
        self.set_model(ls)

        col = gtk.TreeViewColumn("Room Services", gtk.CellRendererText(),
                                 text=0)
        self.append_column(col)

        self.widgets = getWidgets()
        self.sdpdialog = self.widgets.get_widget("sdpdialog")
        self.aname = self.widgets.get_widget("sdp-audio-name")
        self.aaddr = self.widgets.get_widget("sdp-audio-addr")
        self.aport = self.widgets.get_widget("sdp-audio-port")
        self.attl = self.widgets.get_widget("sdp-audio-ttl")
        self.vname = self.widgets.get_widget("sdp-video-name")
        self.vaddr = self.widgets.get_widget("sdp-video-addr")
        self.vport = self.widgets.get_widget("sdp-video-port")
        self.vttl = self.widgets.get_widget("sdp-video-ttl")


        self.connect('button-press-event', self.clicked)
        self.widgets.signal_autoconnect(self)


        # Model event handling
        self.services = services
        services.addReceiver('add', self.add)
        services.addReceiver('remove', self.remove)

        self.xmpp = xmppcomp


    # Double-click handler
    def clicked(self, widget, event):
        if event.type == gtk.gdk._2BUTTON_PRESS:  # Double-click
            path, col, x, y = widget.get_path_at_pos(int(event.x), 
                                                     int(event.y))
            if not path:
                return

            # Lookup user data attached to this row
            model = widget.get_model()
            iter = model.get_iter(path)
            service = model.get_value(iter, 1)
            if not service:
                return

            service.activate()

        elif event.type == gtk.gdk.BUTTON_PRESS and event.button == 3: # Right click
            menu = gtk.Menu()

            item = gtk.MenuItem("Configure SDP")
            item.connect('activate', self.on_sdp_config)
            menu.append(item)

            menu.popup(None, None, None, event.button, event.time)
            menu.show_all()

    def on_sdp_config(self, widget):
        for res in self.services:
            if res.type == 'sdp':
                if res.media == 'audio':
                    self.aname.set_text(res.name) 
                    self.aaddr.set_text(res.addr)
                    self.aport.set_value(float(res.port))
                    self.attl.set_value(float(res.ttl))
                elif res.media == 'video': # Video
                    self.vname.set_text(res.name) 
                    self.vaddr.set_text(res.addr)
                    self.vport.set_value(float(res.port))
                    self.vttl.set_value(float(res.ttl))

        self.sdpdialog.show_all()

    def on_sdpok_clicked(self, widget):
        self.sdpdialog.hide_all()

        arec = MUCService(self.aname.get_text(), 'sdp')
        arec.media = 'audio'
        arec.addr = self.aaddr.get_text()
        arec.port = int(self.aport.get_value())
        arec.ttl = int(self.attl.get_value())
        arec.format = 'rtp-avp-112'

        vrec = MUCService(self.vname.get_text(), 'sdp')
        vrec.media = 'video'
        vrec.addr = self.vaddr.get_text()
        vrec.port = int(self.vport.get_value())
        vrec.ttl = int(self.vttl.get_value())
        vrec.format = 'rtp-avp-31'

        self.xmpp.sendSDP([arec, vrec])

    def on_sdpcancel_clicked(self, widget):
        self.sdpdialog.hide_all()

    def add(self, service):
        ls = self.get_model()
        row = ls.append([service.name, service])

        # Store ref to trackback
        service.udat['listref'] = gtk.TreeRowReference(ls, ls.get_path(row))

    def remove(self, service):
        # Trackback (see above)
        ls = self.get_model()
        ref = service.udat['listref']
        row = ls.get_iter(ref.get_path())
        ls.remove(row)


class MUCChatView(ChatView):
    def __init__(self, conv, xmpp):
        ChatView.__init__(self, conv)

        # Extra ...
        buf = self.tview.get_buffer()
        self.metatag = buf.create_tag('metatag', foreground="#cfcfcf",
                                      style=pango.STYLE_ITALIC)


    def rxChatMsg(self, conv):
        self.log.info("MUC GUI got RX conversation message")
        msg = conv.lastMsg()

        if msg.hidden:
            return

        if msg.sysmsg:
            self.printmsg('*** '+msg.txt+' ***\n', tag=self.metatag)
        else:  # i.e. Is room member
            self.printstamp(msg)
            self.printmsg(msg.sender.resource+": ", tag=self.rxtag)
            self.println(msg.txt)


class MUCGtkComponent(gtk.HPaned, TrivialGTKComponent):
    def __init__(self, conv, xmpp):
        gtk.HPaned.__init__(self)
        TrivialGTKComponent.__init__(self,  conv.contact.name)
        
        self.conv = conv
        self.xmpp = xmpp

        self.chat = MUCChatView(conv, xmpp)
        self.roster = MUCMemberView(conv.roster, xmpp)
        self.services = MUCServiceView(conv.services, xmpp)

        confbut = gtk.Button(label="Configure")
        confbut.connect('clicked', self.reqconfig)
        
        subjbut = gtk.Button(label="Subject")
        subjbut.connect('clicked', self.reqsubj)
        self.subjdialog = SubjectDialog(self.xmpp)

        
        hbb = gtk.HButtonBox()
        hbb.pack_start(subjbut)
        hbb.pack_start(confbut)

        vbox = gtk.VBox()
        vbox.set_property('spacing', 6)
        vbox.set_property('border-width', 6)

        vbox.pack_start(hbb)
        vbox.set_child_packing(hbb, False, True, 0, gtk.PACK_START)
        vbox.pack_start(self.roster)
        vbox.pack_start(self.services)

        self.pack1(self.chat)
        self.pack2(vbox)
        self.set_position(330)

        self.show_all()

        conv.addReceiver('subjectUpdate', self.subjectUpdate)


    def reqconfig(self, widget):
        self.xmpp.reqConfig(self.configCB)

    def subjectUpdate(self, subject):
        self.title = subject

    def configCB(self, form, error=None):
        if error:
            self.conv.addSysMsg("Configuration error: %s" % error)
            return
        xform = XFormDialog(form, self.completedConfig)
        
    def completedConfig(self, form, cancelled=False):
        if cancelled:
            self.conv.addSysMsg("Configuration cancelled")
            self.xmpp.cancelConfig()
        else:
            self.xmpp.sendConfig(form)


    def reqsubj(self, widget):
        self.subjdialog.show_all()

    def close(self):
        self.conv.end()


class MUCGTKPlugin(TrivialGTKPlugin):
    """Main handler for MUC GUI events not attached to particular chat
       session (e.g) menu items"""
    def __init__(self, mucplugin):
        TrivialGTKPlugin.__init__(self)
        self.mucplugin = mucplugin

        # File menu items
        obj = gtk.MenuItem("Join Chat Room")
        obj.connect('activate', self.on_joinchat_activate)
        self.fileitems.append(obj)

        # Roster context items
        self.rosterctxitems.append(self.gen_sendinvite)

        # Hook up to main UI
        #self.view = mucplugin.core.view
        self.account = mucplugin.core.account
        self.xmpp = mucplugin.mucxmpp
        self.xmpp.addReceiver('rxinvite', self.gotInvite)

        self.widgets = getWidgets()
        self.joinmucdialog = self.widgets.get_widget("mucconnectdialog")
        self.mucrecentcombo = self.widgets.get_widget("joinmucrecent")
        self.recent = MUCRecent()        

        # 'Recent' renderer
        cell = gtk.CellRendererText()
        self.mucrecentcombo.pack_start(cell, True)
        self.mucrecentcombo.add_attribute(cell, 'text', 0)

        self.invitedialog = self.widgets.get_widget("invite_dialog")
        self.invitelabel = self.widgets.get_widget("invite_label")
        self.invitemsg = self.widgets.get_widget("invite_msg")
        self.invitecombo = self.widgets.get_widget("invite_combo")
        self.invited_dialog = self.widgets.get_widget("invited")
        self.invited_label = self.widgets.get_widget("invitedlabel")

        # 'Invite' renderer
        cell = gtk.CellRendererText()
        self.invitecombo.pack_start(cell, True)
        self.invitecombo.add_attribute(cell, 'text', 0)

        self.widgets.signal_autoconnect(self)

    def on_joinchat_activate(self, widget):
        self.mucrecentcombo.set_model(self.recent.toListStore())
        self.joinmucdialog.show_all()

    def on_joinmucrecent_changed(self, widget):
        ls = self.mucrecentcombo.get_model()
        jid = ls[self.mucrecentcombo.get_active()][1]

        self.widgets.get_widget('joinmucnick').set_text(jid.resource)
        self.widgets.get_widget('joinmucserver').set_text(jid.host)
        self.widgets.get_widget('joinmucroom').set_text(jid.user)
        self.widgets.get_widget('joinmucpasswd').set_text(jid.passwd or '')

    def on_joinmucok_clicked(self, widget):
        self.joinmucdialog.hide_all()

        nick = self.widgets.get_widget('joinmucnick').get_text()
        server = self.widgets.get_widget('joinmucserver').get_text()
        room = self.widgets.get_widget('joinmucroom').get_text()
        passwd = self.widgets.get_widget('joinmucpasswd').get_text()

        jid = MucJID(tuple=(room, server, nick))
        if passwd:
            jid.passwd = passwd

        self.recent.jids.append(jid)
        self.recent.flush()

        self.mucplugin.createComponent(jid)
        

    def on_joinmuccancel_clicked(self, widget):
        self.joinmucdialog.hide_all()


    def gotInvite(self, invite):
        txt = '<b>%s</b> has invited you to join the chatroom <b>%s</b>' % (invite.sender.full(),
                                                                            invite.room.full())
        if invite.reason:
            txt += '  <b>Message:</b> %s' % invite.reason
        self.invited_label.set_markup(txt)
        self.invited_dialog.set_data('invite', invite)
        self.invited_dialog.show_all()

    def on_invitedcancel_clicked(self, widget):
        invite = self.invited_dialog.get_data('invite')
        self.xmpp.declineInvite(invite)
        self.invited_dialog.hide_all()

    def on_invitedok_clicked(self, widget):
        invite = self.invited_dialog.get_data('invite')
        self.invited_dialog.hide_all()
        
        room = invite.room
        self.widgets.get_widget('joinmucnick').set_text('')
        self.widgets.get_widget('joinmucserver').set_text(room.host)
        self.widgets.get_widget('joinmucroom').set_text(room.user)
        self.widgets.get_widget('joinmucpasswd').set_text(room.passwd or '')
        self.mucrecentcombo.set_model(self.recent.toListStore())
        self.joinmucdialog.show_all()



    def gen_sendinvite(self, contact):
        mi = gtk.MenuItem("Send MUC Invite")
        mi.connect('activate', self.on_sendinvite_activate, contact)
        return mi

    def on_sendinvite_activate(self, widget, contact):
        self.invitelabel.set_label("<b>Invite %s to:</b>" % contact.jid.user)
        ls = gtk.ListStore(gobject.TYPE_STRING,   # JID str
                           gobject.TYPE_PYOBJECT) # Conversation
        for j in self.account.conversations.getAll(type="groupchat"):
            ls.append([j.target.userhost()[0:30], j])
        self.invitecombo.set_model(ls)

        self.invitemsg.get_buffer().set_text("")
        # Attach the contact for later retreival
        self.invitedialog.set_data('contact', contact)
    
        self.invitedialog.show_all()
        
    def on_invitecombo_changed(self, widget):
        ls = widget.get_model()
        conv = ls[widget.get_active()][1]
        self.invitemsg.get_buffer().set_text("Please join us at %s" % conv.target.userhost())

    def on_inviteok_clicked(self, widget):
        # FIXME
        ic = self.invitecombo
        conv = ic.get_model()[ic.get_active()][1]
        if conv:
            contact = self.invitedialog.get_data('contact')
            buf = self.invitemsg.get_buffer()
            (start, end) = buf.get_bounds()
            self.xmpp.sendInvite(conv, contact, reason=buf.get_text(start, end))
        self.invitedialog.hide_all()

    def on_invitecancel_clicked(self, widget):
        self.invitedialog.hide_all()


    def createView(self, conv, xmppcomp):
        view = MUCGtkComponent(conv, xmppcomp)
        self.emitsig('create', component=view)
        return view



class SubjectDialog(gtk.Dialog):
    def __init__(self, xmpp):
        gtk.Dialog.__init__(self, title="Subject Dialog",
                            buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                     gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        self.xmpp = xmpp

        # Setup
        self.set_border_width(12)
        self.vbox.set_spacing(6)

        # Title
        label = gtk.Label()
        label.set_markup('<b>Set Room Subject</b>')
        self.vbox.pack_start(label)

        self.entry = gtk.Entry()
        self.vbox.pack_start(self.entry)
        
        self.connect('response', self.ok)
        self.connect('close', self.cancel)

        self.hide_all()

    def ok(self, dialog, response):
        self.hide_all()

        if response != gtk.RESPONSE_ACCEPT:
            self.cancel(dialog)
            return

        subj = self.entry.get_text()
        self.xmpp.sendSubject(subj)

    def cancel(self, dialog):
        self.hide_all()

