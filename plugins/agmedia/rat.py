
from agmedia.base import MediaProcess

class RatProcess(MediaProcess):
    def __init__(self, account):
        MediaProcess.__init__(self, account)
        self.path = 'rat'

    def serviceAdd(self, service):
        self.log.info("Rat received service add")
        if service.media == 'audio':
            service.addReceiver('activate', self.activate)

    def genParams(self):
        parms = ['-t '+self.service.ttl, self.service.addr+'/'+self.service.port]
        if self.service.enctype:
            parms.append('-crypt '+self.service.enckey)
        return [self.path] + parms
